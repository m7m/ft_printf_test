
# include "ft_printf_test.h"

void	maintest(void)
{
	int i, j;

	# if SETLOCAL_ENABLE != 0
	clock_t start, finish;
	start = clock();
	# endif

	#if TFLOAT == 1
	FT_PRINTF_TEST("----------------------------------------------\n");
	FT_PRINTF_TEST("%a %a %a", 0., 1., 2.);
	FT_PRINTF_TEST("%a %a %a", 3., 4., 5.);
	FT_PRINTF_TEST("%a %a %a", 6., 7., 8.);
	FT_PRINTF_TEST("%a %a %a", 9., 10., 11.);
	FT_PRINTF_TEST("%f %f %f", 0., 1., 2.);
	FT_PRINTF_TEST("%f %f %f", 3., 4., 5.);
	FT_PRINTF_TEST("%f %f %f", 6., 7., 8.);
	FT_PRINTF_TEST("%f %f %f", 9., 10., 11.);
	FT_PRINTF_TEST("%e %e %e", 0., 1., 2.);
	FT_PRINTF_TEST("%e %e %e", 3., 4., 5.);
	FT_PRINTF_TEST("%e %e %e", 6., 7., 8.);
	FT_PRINTF_TEST("%e %e %e", 9., 10., 11.);
	FT_PRINTF_TEST("%g %g %g", 0., 1., 2.);
	FT_PRINTF_TEST("%g %g %g", 3., 4., 5.);
	FT_PRINTF_TEST("%g %g %g", 6., 7., 8.);
	FT_PRINTF_TEST("%g %g %g", 9., 10., 11.);

	double dblnan;
	char *strdblnan = (char *)&dblnan;
	dblnan = 12.3;
	strdblnan[sizeof(double) - 1] = 0b01111111;
	strdblnan[sizeof(double) - 2] = 0b11110000;
	FT_PRINTF_TEST("%a %A", dblnan, dblnan);
	FT_PRINTF_TEST("%e %E", dblnan, dblnan);
	FT_PRINTF_TEST("%f %F", dblnan, dblnan);
	FT_PRINTF_TEST("%g %G", dblnan, dblnan);
	dblnan = 0;
	strdblnan[sizeof(double) - 1] = 0b01111111;
	strdblnan[sizeof(double) - 2] = 0b11110000;
	FT_PRINTF_TEST("%a %A", dblnan, dblnan);
	FT_PRINTF_TEST("%e %E", dblnan, dblnan);
	FT_PRINTF_TEST("%f %F", dblnan, dblnan);
	FT_PRINTF_TEST("%g %G", dblnan, dblnan);
	dblnan = 0;
	strdblnan[sizeof(double) - 1] = 0b11111111;
	strdblnan[sizeof(double) - 2] = 0b11110000;
	FT_PRINTF_TEST("%a %A", dblnan, dblnan);
	FT_PRINTF_TEST("%e %E", dblnan, dblnan);
	FT_PRINTF_TEST("%f %F", dblnan, dblnan);
	FT_PRINTF_TEST("%g %G", dblnan, dblnan);
	dblnan = -12.3;
	strdblnan[sizeof(double) - 1] = 0b01111111;
	strdblnan[sizeof(double) - 2] = 0b11110000;
	FT_PRINTF_TEST("%a %A", dblnan, dblnan);
	FT_PRINTF_TEST("%e %E", dblnan, dblnan);
	FT_PRINTF_TEST("%f %F", dblnan, dblnan);
	FT_PRINTF_TEST("%g %G", dblnan, dblnan);
	int sd = sizeof(double);
	while (sd--)
		strdblnan[sd] = 0b11111111;
	FT_PRINTF_TEST("%a %A", dblnan, dblnan);
	FT_PRINTF_TEST("%e %E", dblnan, dblnan);
	FT_PRINTF_TEST("%f %F", dblnan, dblnan);
	FT_PRINTF_TEST("%g %G", dblnan, dblnan);
	sd = sizeof(double) - 2;
	strdblnan[sizeof(double) - 1] = 0b00000000;
	strdblnan[sizeof(double) - 2] = 0b00001111;
	while (sd--)
		strdblnan[sd] = 0b11111111;
	#ifdef __linux__
	FT_PRINTF_TEST("%a %A", dblnan, dblnan);
	#endif
	sd = sizeof(double);
	while (sd--)
		strdblnan[sd] = 0b00000000;
	strdblnan[1] = 0b00000001;
	#ifdef __linux__
	FT_PRINTF_TEST("%a %A", dblnan, dblnan);
	#endif
	
//	FT_PRINTF_TEST("%e %E", dblnan, dblnan); // valeur differente
	FT_PRINTF_TEST("%f %F", dblnan, dblnan);
//	FT_PRINTF_TEST("%g %G", dblnan, dblnan); // valeur differente

	FT_PRINTF_TEST("%a %A", DBL_MIN, DBL_MIN);
	FT_PRINTF_TEST("%e %E", DBL_MIN, DBL_MIN);
	FT_PRINTF_TEST("%f %F", DBL_MIN, DBL_MIN);
	FT_PRINTF_TEST("%g %G", DBL_MIN, DBL_MIN);

	strdblnan[sizeof(double) - 1] = 0b01111111;
	FT_PRINTF_TEST("%a %A", dblnan, dblnan);
	FT_PRINTF_TEST("%e %E", dblnan, dblnan);
//	FT_PRINTF_TEST("%f %F", dblnan, dblnan); // valeur differente
	FT_PRINTF_TEST("%g %G", dblnan, dblnan);
	
	// attention overflow
	/* FT_PRINTF_TEST("%a %A", 0X1.FFFFFFFFFFFFFP+1024, 0X1.FFFFFFFFFFFFFP+1024); */
	/* FT_PRINTF_TEST("%e %E", 0X1.FFFFFFFFFFFFFP+1024, 0X1.FFFFFFFFFFFFFP+1024); */
	/* FT_PRINTF_TEST("%f %F", 0X1.FFFFFFFFFFFFFP+1024, 0X1.FFFFFFFFFFFFFP+1024); */
	/* FT_PRINTF_TEST("%g %G", 0X1.FFFFFFFFFFFFFP+1024, 0X1.FFFFFFFFFFFFFP+1024); */
	/* FT_PRINTF_TEST("%a %A", -0X1.FFFFFFFFFFFFFP+1024, -0X1.FFFFFFFFFFFFFP+1024); */
	/* FT_PRINTF_TEST("%e %E", -0X1.FFFFFFFFFFFFFP+1024, -0X1.FFFFFFFFFFFFFP+1024); */
	/* FT_PRINTF_TEST("%f %F", -0X1.FFFFFFFFFFFFFP+1024, -0X1.FFFFFFFFFFFFFP+1024); */
	/* FT_PRINTF_TEST("%g %G", -0X1.FFFFFFFFFFFFFP+1024, -0X1.FFFFFFFFFFFFFP+1024); */
	/* FT_PRINTF_TEST("%a %A", 0X1.00000200000000000000P+1025, 0X1.FFFFFFFFFFFFFP+1025); */
	/* FT_PRINTF_TEST("%a %A", 0X1.99999999999AP+2047, 0X1.99999999999AP+2047); */
	/* FT_PRINTF_TEST("%a %A", 0x8FFFFFFFFFFFFFFF, 0x7FF099999999999A); */
	/* FT_PRINTF_TEST("%A", 0X1.0000000000000P+1024);//nan */
	FT_PRINTF_TEST("%G", 0.000000000000000100023456);
	FT_PRINTF_TEST("%G", 0.0000000000000001000023456);
	FT_PRINTF_TEST("%G", 0.00000000000000010000023456);
	FT_PRINTF_TEST("%G", 0.000000000000000100000023456);
	FT_PRINTF_TEST("%G", 0.0000000000000001000000023456);
	FT_PRINTF_TEST("%g %g %g", 1., 0., 123456.);
	FT_PRINTF_TEST("%g %g %g", 12345.6, 1234.56, 123.456);
	FT_PRINTF_TEST("%g %g %g", 12.3456, 1.23456, 0.123456);
	FT_PRINTF_TEST("%g %g", 12345678901234.4567890123, 123456789012344567890123.);
	FT_PRINTF_TEST("%g", 123456789.);
	FT_PRINTF_TEST("%g", 12345678.);
	FT_PRINTF_TEST("%g", 1234567.);
	FT_PRINTF_TEST("%g", 123456.);
	FT_PRINTF_TEST("%g", 12345.);
	FT_PRINTF_TEST("%g", 1234.);

	FT_PRINTF_TEST("%g %g %g", 0.000123456, 0.0000123456, 0.00000000000000000000000000000001);
	FT_PRINTF_TEST("%.0g %.1g %.2g", 123.456, 123.456, 123.456);
	FT_PRINTF_TEST("%.3g %.4g %.5g", 123.456, 123.456, 123.456);
	FT_PRINTF_TEST("%.8g %.15g %g", 123.456, 123.456, 123.456);
	FT_PRINTF_TEST("%.6g%g", 123.456, 123.456, 123.456);
	FT_PRINTF_TEST("%.0g %.1g %.2g", 5555.55555555555, 5555.55555555555, 5555.55555555555);
	FT_PRINTF_TEST("%.3g %.4g %.5g", 5555.55555555555, 5555.55555555555, 5555.55555555555);
	FT_PRINTF_TEST("%.8g %.15g %g", 5555.55555555555, 5555.55555555555, 5555.55555555555);
	FT_PRINTF_TEST("%.0g %.1g %.2g", 6666.66666666666, 6666.66666666666, 6666.66666666666);
	FT_PRINTF_TEST("%.3g %.4g %.5g", 6666.66666666666, 6666.66666666666, 6666.66666666666);
	FT_PRINTF_TEST("%.8g %.15g %g", 6666.66666666666, 6666.66666666666, 6666.66666666666);
	FT_PRINTF_TEST("%.3g %.4g %.5g", -6666.66666666666, -6666.66666666666, -6666.66666666666);
	FT_PRINTF_TEST("%.8g %.15g %g", -6666.66666666666, -6666.66666666666, -6666.66666666666);
	FT_PRINTF_TEST("%.8g", -6666.66666666666);
	FT_PRINTF_TEST("%.20g", -6666.66666666666);
	
	FT_PRINTF_TEST("%.0g %.1g %.2g", 0., 0., 0.);
	FT_PRINTF_TEST("%.3g %.4g %.5g", 0., 0., 0.);
	FT_PRINTF_TEST("%.8g %.15g %g", 0., 0., 0.);
	FT_PRINTF_TEST("%.0g %.1g %.2g", 1., 1., 1.);
	FT_PRINTF_TEST("%.3g %.4g %.5g", 1., 1., 1.);
	FT_PRINTF_TEST("%.8g %.15g %g", 1., 1., 1.);
	
	FT_PRINTF_TEST("%E", 0.000000000000000100000023456);
	FT_PRINTF_TEST("%E", 0.0000000000000001000000023456);

	FT_PRINTF_TEST("%a %a %a %a", 12.3456, 1.23456, 0.123456, 0.000123456);
	FT_PRINTF_TEST("%a %a %a", 0.0000123456, 0.00000000000000000000000000000001, 10000000000000000000000000000000.);
	FT_PRINTF_TEST("%a %a %a", 1., 0., 123456.);
	FT_PRINTF_TEST("%a %a %a", 12345.6, 1234.56, 123.456);
	FT_PRINTF_TEST("%a %a %a", 123.456, 12.3456, 1.23456);
	FT_PRINTF_TEST("%a %a %a", 0.123456, 0.000123456, -0.000123456);
	FT_PRINTF_TEST("%a %a", 0.00123456789123456789, 0.0000123456789);
	FT_PRINTF_TEST("%a %a", 1234567891234567890000., 0.000123456789);

	FT_PRINTF_TEST("%lA", 10.);
	/* #ifdef __linux__ */
	/* FT_PRINTF_TEST("%lA", LDBL_MAX); */
	/* #endif */
	FT_PRINTF_TEST("%hA", 10.);
	FT_PRINTF_TEST("%hhA", 10.);
	/* #ifdef __linux__ */
	/* FT_PRINTF_TEST("%hhA", LDBL_MAX); */
	/* #endif */
//	FT_PRINTF_TEST("%llA", (long double)10.); // long double
//	FT_PRINTF_TEST("%llA", LDBL_MAX); // long double
	FT_PRINTF_TEST("%jA", 10.);
	FT_PRINTF_TEST("%jA", DBL_MIN);
	FT_PRINTF_TEST("%jA", DBL_MAX);
	/* #ifdef __linux__ */
	/* FT_PRINTF_TEST("%jA", LDBL_MAX); */
	/* FT_PRINTF_TEST("%jA", LDBL_MIN); */
	/* #endif */
	FT_PRINTF_TEST("%zA", 10.);
	FT_PRINTF_TEST("%zA", DBL_MIN);
	FT_PRINTF_TEST("%zA", DBL_MAX);
	/* #ifdef __linux__ */
	/* FT_PRINTF_TEST("%zA", LDBL_MAX); */
	/* FT_PRINTF_TEST("%zA", LDBL_MIN); */
	/* #endif */
//	FT_PRINTF_TEST("%qA", (long double)10.); // long double
//	FT_PRINTF_TEST("%qA", DBL_MAX);
//	FT_PRINTF_TEST("%qA", DBL_MIN);
//	FT_PRINTF_TEST("%qA", LDBL_MIN);
//	FT_PRINTF_TEST("%qA", LDBL_MAX);
	FT_PRINTF_TEST("%tA", 10.);
	FT_PRINTF_TEST("%tA", DBL_MIN);
	FT_PRINTF_TEST("%tA", DBL_MAX);
	/* #ifdef __linux__ */
	/* FT_PRINTF_TEST("%tA", LDBL_MIN); */
	/* FT_PRINTF_TEST("%tA", LDBL_MAX); */
	/* #endif */


	FT_PRINTF_TEST("%e %e %e %e", 12.3456, 1.23456, 0.123456, 0.000123456);
	FT_PRINTF_TEST("%e %e %e", 0.0000123456, 0.00000000000000000000000000000001, 10000000000000000000000000000000.);
	FT_PRINTF_TEST("%e %e %e", 1., 0., 123456.);
	FT_PRINTF_TEST("%e %e %e", 12345.6, 1234.56, 123.456);
	FT_PRINTF_TEST("%e %e %e", 123.456, 12.3456, 1.23456);
	FT_PRINTF_TEST("%e %e %e", 0.123456, 0.000123456, -0.000123456);
	FT_PRINTF_TEST("%e %e %e %e", 0.00123456789123456789, 1234567891234567890000., 0.0000123456789, 0.000123456789);
	FT_PRINTF_TEST("%.0e %.1e %.2e", 123.456, 123.456, 123.456);
	FT_PRINTF_TEST("%.3e %.4e %.5e", 123.456, 123.456, 123.456);
	FT_PRINTF_TEST("%.8e %.15e %e", 123.456, 123.456, 123.456);
	FT_PRINTF_TEST("%.0e %.1e %.2e", 5555.55555555555, 5555.55555555555, 5555.55555555555);
	FT_PRINTF_TEST("%.3e %.4e %.5e", 5555.55555555555, 5555.55555555555, 5555.55555555555);
	FT_PRINTF_TEST("%.8e %.15e %e", 5555.55555555555, 5555.55555555555, 5555.55555555555);
	FT_PRINTF_TEST("%.0e %.1e %.2e", 6666.66666666666, 6666.66666666666, 6666.66666666666);
	FT_PRINTF_TEST("%.3e %.4e %.5e", 6666.66666666666, 6666.66666666666, 6666.66666666666);
	FT_PRINTF_TEST("%.8e %.15e %e", 6666.66666666666, 6666.66666666666, 6666.66666666666);
	FT_PRINTF_TEST("%.3e %.4e %.5e", -6666.66666666666, -6666.66666666666, -6666.66666666666);
	FT_PRINTF_TEST("%.8e %.15e %e", -6666.66666666666, -6666.66666666666, -6666.66666666666);
	FT_PRINTF_TEST("%.0e %.1e %.2e", 0., 0., 0.);
	FT_PRINTF_TEST("%.3e %.4e %.5e", 0., 0., 0.);
	FT_PRINTF_TEST("%.8e %.15e %e", 0., 0., 0.);
	FT_PRINTF_TEST("%.0e %.1e %.2e", 1., 1., 1.);
	FT_PRINTF_TEST("%.3e %.4e %.5e", 1., 1., 1.);
	FT_PRINTF_TEST("%.8e %.15e %e", 1., 1., 1.);
	FT_PRINTF_TEST("%.e", 0.01);
	FT_PRINTF_TEST("%.e", -0.01);
	FT_PRINTF_TEST("%.e", 0.05);
	FT_PRINTF_TEST("%.e", -0.05);
	FT_PRINTF_TEST("%.e", 0.09);
	FT_PRINTF_TEST("%.e", -0.09);
	FT_PRINTF_TEST("%lE", 10.);
//	FT_PRINTF_TEST("%lE", LDBL_MAX); // valeur differente
	FT_PRINTF_TEST("%hE", 10.);
	FT_PRINTF_TEST("%hhE", 10.);
//	FT_PRINTF_TEST("%hhE", LDBL_MAX); // valeur differente
//	FT_PRINTF_TEST("%llE", (long double)10.); // long double
//	FT_PRINTF_TEST("%llE", LDBL_MAX); // long double
	FT_PRINTF_TEST("%jE", 10.);
	FT_PRINTF_TEST("%jE", DBL_MIN);
	FT_PRINTF_TEST("%jE", DBL_MAX);
//	FT_PRINTF_TEST("%jE", LDBL_MAX); // valeur differente
//	FT_PRINTF_TEST("%jE", LDBL_MIN); // valeur differente
	FT_PRINTF_TEST("%zE", 10.);
	FT_PRINTF_TEST("%zE", DBL_MIN);
	FT_PRINTF_TEST("%zE", DBL_MAX);
//	FT_PRINTF_TEST("%zE", LDBL_MAX); // valeur differente
//	FT_PRINTF_TEST("%zE", LDBL_MIN); // valeur differente
//	FT_PRINTF_TEST("%qE", (long double)10.); // long double
//	FT_PRINTF_TEST("%qE", DBL_MAX);
//	FT_PRINTF_TEST("%qE", DBL_MIN);
//	FT_PRINTF_TEST("%qE", LDBL_MIN);
//	FT_PRINTF_TEST("%qE", LDBL_MAX);
	FT_PRINTF_TEST("%tE", 10.);
	FT_PRINTF_TEST("%tE", DBL_MIN);
	FT_PRINTF_TEST("%tE", DBL_MAX);
//	FT_PRINTF_TEST("%tE", LDBL_MIN); // valeur differente
//	FT_PRINTF_TEST("%tE", LDBL_MAX); // valeur differente

	FT_PRINTF_TEST("%f %f %f %f", 12.3456, 1.23456, 0.123456, 0.000123456);
	FT_PRINTF_TEST("%f %f", 0.0000123456, 0.00000000000000000000000000000001);
//	FT_PRINTF_TEST("%f", 10000000000000000000000000000000.); // valeur differente
	FT_PRINTF_TEST("%f %f %f", 1., 0., 123456.);
	FT_PRINTF_TEST("%f %f %f", 12345.6, 1234.56, 123.456);
	FT_PRINTF_TEST("%f %f %f", 123.456, 12.3456, 1.23456);
	FT_PRINTF_TEST("%f %f %f", 0.123456, 0.000123456, -0.000123456);
	FT_PRINTF_TEST("%f %f %f %f", 0.00123456789123456789, 1234567891234567890000., 0.0000123456789, 0.000123456789);

	
	FT_PRINTF_TEST("%g %g %g %g", 12.3456, 1.23456, 0.123456, 0.000123456);
	FT_PRINTF_TEST("%g %g %g", 0.0000123456, 0.00000000000000000000000000000001, 10000000000000000000000000000000.);
	FT_PRINTF_TEST("%g %g %g", 1., 0., 123456.);
	FT_PRINTF_TEST("%g %g %g", 12345.6, 1234.56, 123.456);
	FT_PRINTF_TEST("%g %g %g", 123.456, 12.3456, 1.23456);
	FT_PRINTF_TEST("%g %g %g", 0.123456, 0.000123456, -0.000123456);
	FT_PRINTF_TEST("%g %g %g %g", 0.00123456789123456789, 1234567891234567890000., 0.0000123456789, 0.000123456789);
	FT_PRINTF_TEST("%lG", 10.);
//	FT_PRINTF_TEST("%lG", LDBL_MAX); // valeur differente
	FT_PRINTF_TEST("%hG", 10.);
	FT_PRINTF_TEST("%hhG", 10.);
//	FT_PRINTF_TEST("%hhG", LDBL_MAX); // valeur differente
//	FT_PRINTF_TEST("%llG", (long double)10.); // long double
//	FT_PRINTF_TEST("%llG", LDBL_MAX);
	FT_PRINTF_TEST("%jG", 10.);
	FT_PRINTF_TEST("%jG", DBL_MIN);
	FT_PRINTF_TEST("%jG", DBL_MAX);
//	FT_PRINTF_TEST("%jG", LDBL_MAX); // valeur differente
//	FT_PRINTF_TEST("%jG", LDBL_MIN); // valeur differente
	FT_PRINTF_TEST("%zG", 10.);
	FT_PRINTF_TEST("%zG", DBL_MIN);
	FT_PRINTF_TEST("%zG", DBL_MAX);
//	FT_PRINTF_TEST("%zG", LDBL_MAX);
//	FT_PRINTF_TEST("%zG", LDBL_MIN);
//	FT_PRINTF_TEST("%qG", (long double)10.);  // long double
//	FT_PRINTF_TEST("%qG", DBL_MAX);
//	FT_PRINTF_TEST("%qG", DBL_MIN);
//	FT_PRINTF_TEST("%qG", LDBL_MIN);
//	FT_PRINTF_TEST("%qG", LDBL_MAX);
	FT_PRINTF_TEST("%tG", 10.);
	FT_PRINTF_TEST("%tG", DBL_MIN);
	FT_PRINTF_TEST("%tG", DBL_MAX);
//	FT_PRINTF_TEST("%tG", LDBL_MIN); // valeur differente
//	FT_PRINTF_TEST("%tG", LDBL_MAX); // valeur differente
	
	FT_PRINTF_TEST("----------------------------------------------\n");

	FT_PRINTF_TEST("%20.4a", 0.78999);
	FT_PRINTF_TEST("%020.4a", 0.78999);
	FT_PRINTF_TEST("%20.4a", -0.78999);
	FT_PRINTF_TEST("%020.4a", -0.78999);
	FT_PRINTF_TEST("%.3f", 0.78999);
	FT_PRINTF_TEST("%10.3a", 0.78999);
	FT_PRINTF_TEST("%010.3a", 0.78999);
	FT_PRINTF_TEST("%10.4a", 0.78999);
	FT_PRINTF_TEST("%010.4a", 0.78999);
	FT_PRINTF_TEST("%10.5a", 0.78999);
	FT_PRINTF_TEST("%010.5a", 0.78999);
	FT_PRINTF_TEST("%10a", 0.78999);
	FT_PRINTF_TEST("%a", 0.78999);
	FT_PRINTF_TEST("%.20a", 0.78999);
	FT_PRINTF_TEST("%.20a", 0X1P+0);
	FT_PRINTF_TEST("%.15a", 0X1P+0);
	FT_PRINTF_TEST("%a", -0X0P+0);
	FT_PRINTF_TEST("%a", 0X1P+0);
	FT_PRINTF_TEST("%a", 0X5P+0);
	FT_PRINTF_TEST("%.0a", 0X1P+0);
	FT_PRINTF_TEST("%.0a", 0X5P+0);
	FT_PRINTF_TEST("%a", 0X1.1P+0);
	FT_PRINTF_TEST("%a", 0X1.5P+0);
	FT_PRINTF_TEST("%.0a", 0X1.1P+0);
	FT_PRINTF_TEST("%.0a", 0X1.5P+0);
	FT_PRINTF_TEST("%.0a", 0.78999);

	FT_PRINTF_TEST("%.12a", 0.78999);
	FT_PRINTF_TEST("%.13a", 0.78999);
	FT_PRINTF_TEST("%.14a", 0.78999);
	FT_PRINTF_TEST("%.16a", 0.78999);
	FT_PRINTF_TEST("%a", 0X1.1111111111111P+0);
	FT_PRINTF_TEST("%.5a", 0X1.1111111111111P+0);
	FT_PRINTF_TEST("%.12a", 0X1.1111111111111P+0);
	FT_PRINTF_TEST("%.13a", 0X1.1111111111111P+0);
	FT_PRINTF_TEST("%.14a", 0X1.1111111111111P+0);
	FT_PRINTF_TEST("%.16a", 0X1.1111111111111P+0);
	FT_PRINTF_TEST("%.5a", 0X1.1010101010101P+0);
	FT_PRINTF_TEST("%.5a", 0X1.0101010100101P+0);
	FT_PRINTF_TEST("%.5a", 0X1.F7F7F7F7F7F7FP+0);
	FT_PRINTF_TEST("%.5a", 0X1.7F7F7F7F7F7F7P+0);
	FT_PRINTF_TEST("%.5a", 0X1.F8F8F8F8F8F8FP+0);
	FT_PRINTF_TEST("%.5a", 0X1.8F8F8F8F8F8F8P+0);
	sd = sizeof(double) - 2;
	strdblnan[sizeof(double) - 1] = 0b00000000;
	strdblnan[sizeof(double) - 2] = 0b00001111;
	while (sd--)
		strdblnan[sd] = 0b11111111;
	FT_PRINTF_TEST("%.0a %.0A", dblnan, dblnan);

	FT_PRINTF_TEST("%20.4e", 0.78999);
	FT_PRINTF_TEST("%020.4e", 0.78999);
	FT_PRINTF_TEST("%10.3e", 0.78999);
	FT_PRINTF_TEST("%010.3e", 0.78999);
	FT_PRINTF_TEST("%10.3f", 0.78999);
	FT_PRINTF_TEST("%010.3f", 0.78999);
	FT_PRINTF_TEST("%20.4g", 0.78999);
	FT_PRINTF_TEST("%020.4g", 0.78999);

	FT_PRINTF_TEST("%10.3g", 0.78999);
	FT_PRINTF_TEST("%010.3g", 0.78999);
	FT_PRINTF_TEST("%10.3g", -0.78999);
	FT_PRINTF_TEST("%010.3g", -0.78999);
	/* FT_PRINTF_TEST("%f", 1234567.89123); */
	/* FT_PRINTF_TEST("|% .0f|% 10f|% d", 1234567.89123, 1234567.89123, -20); */
	/* FT_PRINTF_TEST("0 |% 050f", 1234567.89); */
	/* FT_PRINTF_TEST("0 |% 050f", -1234567.89); */
	/* FT_PRINTF_TEST("0 |% 50f", -1234567.89); */
	/* FT_PRINTF_TEST("+ |%+050f|%+050f", 1234567.89, -1234567.89); */
	/* FT_PRINTF_TEST("0 |%050.2f", 1234567.89); */
	/* FT_PRINTF_TEST("# |%#50.5f", (double)10); */
	/* FT_PRINTF_TEST("%.f %.0f %.10f %.f %.1f", 12.34, 12., 12., 12.5, 12.46); */
	/* FT_PRINTF_TEST("%g %g %g %g %g %f", 1., 0., 123456., 12345.6, 1234.56, 123.456); */
	/* FT_PRINTF_TEST("%*.f|%.*f", 5, 10.123, 3, 10.1236); */
	/* FT_PRINTF_TEST("%.5f", 1234567.89); */
	/* FT_PRINTF_TEST("%-.10f", 10.); */

	/* FT_PRINTF_TEST("%33.0f", 1234567890123.4567890123456789); */
	/* FT_PRINTF_TEST("%33.1f", 1234567890123.4567890123456789); */
	/* FT_PRINTF_TEST("%033.0f", 1234567890123.4567890123456789); */
	/* FT_PRINTF_TEST("%033.1f", 1234567890123.4567890123456789); */
	/* FT_PRINTF_TEST("% 033.0f", 1234567890123.4567890123456789); */
	/* FT_PRINTF_TEST("% 033.1f", 1234567890123.4567890123456789); */
	/* FT_PRINTF_TEST("%# 033.0f", 1234567890123.4567890123456789); */
	/* FT_PRINTF_TEST("%# 033.1f", 1234567890123.4567890123456789); */
	/* FT_PRINTF_TEST("%# 033.0f", 1234567890123.); */
	/* FT_PRINTF_TEST("%#33.0f", 1234567890123.); */
	/* FT_PRINTF_TEST("%#.0f", 1234567890123.); */
	/* FT_PRINTF_TEST("%#f", 1234567890123.); */
	/* FT_PRINTF_TEST("%####0000 33..1..#00f", 1234567890123.4567890123456789); */
	FT_PRINTF_TEST("{%f}{%F}", 1444565444646.6465424242242, 1444565444646.6465424242242);
	FT_PRINTF_TEST("{%f}{%F}", -1444565444646.6465424242242454654, -1444565444646.6465424242242454654);
	FT_PRINTF_TEST("%.3a", 15.);
	FT_PRINTF_TEST("%05a", 15.);
	FT_PRINTF_TEST("%.10g", 0.01);
	FT_PRINTF_TEST("%.21g", 0X1P-4);

	FT_PRINTF_TEST("%.3f", (double)10.666);
	FT_PRINTF_TEST("%.3f", (double)10.);

	#if TETOI == 1
	FT_PRINTF_TEST("%.*1$s", "aze", "rty");
	FT_PRINTF_TEST("%20.*d", -5, 4);
	FT_PRINTF_TEST("%20.*d", -25, 4);
	FT_PRINTF_TEST("%20.*d", -20, 4);
	FT_PRINTF_TEST("%05.*d", -15, 42);
	FT_PRINTF_TEST("%05.*d", -4, 42);
	FT_PRINTF_TEST("%05.*d", -5, 42);
	FT_PRINTF_TEST("%05.*d", 15, 42);
	FT_PRINTF_TEST("%05.*d", 4, 42);
	FT_PRINTF_TEST("%05.*d", 5, 42);

	FT_PRINTF_TEST("%020.*d", -15, 42);
	FT_PRINTF_TEST("%020.*d", -4, 42);
	FT_PRINTF_TEST("%020.*d", -5, 42);
	FT_PRINTF_TEST("%.*f", 3, (double)10.123);
	FT_PRINTF_TEST("%.*f", -3, (double)10.666);
	FT_PRINTF_TEST("%.*f", -7, (double)10.666);
	FT_PRINTF_TEST("%.*f", 13, (double)10.123);
	FT_PRINTF_TEST("%.*f", -13, (double)10.666);
	FT_PRINTF_TEST("%20.*g", -5, 0.01);
	FT_PRINTF_TEST("%20.*e", -5, 0.01);
	FT_PRINTF_TEST("%20.*a", -5, 0.01);
	FT_PRINTF_TEST("%20.*f", -5, 0.01);
	FT_PRINTF_TEST("%.*g", -5, 0.01);
	FT_PRINTF_TEST("%.*e", -5, 0.01);
	FT_PRINTF_TEST("%.*a", -5, 0.01);
	FT_PRINTF_TEST("%.*f", -5, 0.01);
	FT_PRINTF_TEST("%20.*g", -5, 0X1P-4);
	FT_PRINTF_TEST("%20.*e", -5, 0X1P-4);
	FT_PRINTF_TEST("%20.*a", -5, 0X1P-4);
	FT_PRINTF_TEST("%20.*f", -5, 0X1P-4);
	FT_PRINTF_TEST("%20.*g", 5, 0.01);
	FT_PRINTF_TEST("%20.*e", 5, 0.01);
	FT_PRINTF_TEST("%20.*a", 5, 0.01);
	FT_PRINTF_TEST("%20.*f", 5, 0.01);
	FT_PRINTF_TEST("%20.*g", 5, 0.01234567);
	FT_PRINTF_TEST("%20.*e", 5, 0.01234567);
	FT_PRINTF_TEST("%20.*a", 5, 0.01234567);
	FT_PRINTF_TEST("%20.*f", 5, 0.01234567);
	FT_PRINTF_TEST("%20.*g", -5, 0.01234567);
	FT_PRINTF_TEST("%20.*e", -5, 0.01234567);
	FT_PRINTF_TEST("%20.*a", -5, 0.01234567);
	FT_PRINTF_TEST("%20.*f", -5, 0.01234567);

	FT_PRINTF_TEST("%19.*g", -5, 0.01);
	FT_PRINTF_TEST("%19.*e", -5, 0.01);
	FT_PRINTF_TEST("%19.*a", -5, 0.01);
	FT_PRINTF_TEST("%19.*f", -5, 0.01);
	FT_PRINTF_TEST("%19.*g", 5, 0.01);
	FT_PRINTF_TEST("%19.*e", 5, 0.01);
	FT_PRINTF_TEST("%19.*a", 5, 0.01);
	FT_PRINTF_TEST("%19.*f", 5, 0.01);

	FT_PRINTF_TEST("%18.*g", -5, 0.01);
	FT_PRINTF_TEST("%18.*e", -5, 0.01);
	FT_PRINTF_TEST("%18.*a", -5, 0.01);
	FT_PRINTF_TEST("%18.*f", -5, 0.01);
	FT_PRINTF_TEST("%18.*g", 5, 0.01);
	FT_PRINTF_TEST("%18.*e", 5, 0.01);
	FT_PRINTF_TEST("%18.*a", 5, 0.01);
	FT_PRINTF_TEST("%18.*f", 5, 0.01);
	FT_PRINTF_TEST("%21.*g", -5, 0.01);
	FT_PRINTF_TEST("%21.*e", -5, 0.01);
	FT_PRINTF_TEST("%21.*a", -5, 0.01);
	FT_PRINTF_TEST("%21.*f", -5, 0.01);
	FT_PRINTF_TEST("%21.*g", 5, 0.01);
	FT_PRINTF_TEST("%21.*e", 5, 0.01);
	FT_PRINTF_TEST("%21.*a", 5, 0.01);
	FT_PRINTF_TEST("%21.*f", 5, 0.01);
	
	#endif /* TETOI FLOAT*/

	#if TDOL == 1
	FT_PRINTF_TEST("%.*1$f", (double)10.123, 22., 33., 44.);
	FT_PRINTF_TEST("%.*1$f", (double)10.666, 22., 33., 44.);
	FT_PRINTF_TEST("%.*1$f", (double)10.666, 2., 33., 44.);
	FT_PRINTF_TEST("%2$.*1$f", 5, 10.1236);
	FT_PRINTF_TEST("%.*2$f", (double)10.123, 3, 33., 44.);
	FT_PRINTF_TEST("%.*2$f", (double)10.123, 7, 33., 44.);
	FT_PRINTF_TEST("%.*2$f", (double)10.123, 6, 33., 44.);
	FT_PRINTF_TEST("%.*2$f", (double)10.666, -3, 33., 44.);
	FT_PRINTF_TEST("%.*2$f", (double)10.666, -7, 33., 44.);
	FT_PRINTF_TEST("%.*2$f", (double)10.123, 13, 33., 44.);
	FT_PRINTF_TEST("%.*2$f", (double)10.666, -13, 33., 44.);
	FT_PRINTF_TEST("%2$.*1$f", 5, 10.555555);
	FT_PRINTF_TEST(". |%1$.*2$f", (double)10.123, 10);
	FT_PRINTF_TEST("%.1$g", -5., 0.01);
	FT_PRINTF_TEST("%.1$e", -5., 0.01);
	FT_PRINTF_TEST("%.1$a", -5., 0.01);
	FT_PRINTF_TEST("%.1$f", -5., 0.01);
	FT_PRINTF_TEST("%.*1$g", -5., 0.01);

	/* a vos risques est perils */
	/* FT_PRINTF_TEST("%.*1$e", -5., 0.01); */
	/* FT_PRINTF_TEST("%.*1$a", -5., 0.01); */
	/* FT_PRINTF_TEST("%.*1$f", -5., 0.01); */
	/* FT_PRINTF_TEST("%.*1$g", 2., 0.01); */
	/* FT_PRINTF_TEST("%.*1$e", 2., 0.01); */
	/* FT_PRINTF_TEST("%.*1$a", 2., 0.01); */
	/* FT_PRINTF_TEST("%.*1$f", 2., 0.01); */
	/* FT_PRINTF_TEST("%.*1$g", 1.2345678, 0.01); */
	/* FT_PRINTF_TEST("%.*1$e", 1.2345678, 0.01); */
	/* FT_PRINTF_TEST("%.*1$a", 1.2345678, 0.01); */
	/* FT_PRINTF_TEST("%.*1$f", 1.2345678, 0.01); */
	/* FT_PRINTF_TEST("%.*1$g", 2.123, 0.01); */
	/* FT_PRINTF_TEST("%.*1$e", 2.123, 0.01); */
	/* FT_PRINTF_TEST("%.*1$a", 2.123, 0.01); */
	/* FT_PRINTF_TEST("%.*1$f", 2.123, 0.01); */
	/* FT_PRINTF_TEST("%.*1$g", 2., -5); */
	/* FT_PRINTF_TEST("%.*1$e", 2., -5); */
	/* FT_PRINTF_TEST("%.*1$a", 2., -5); */
	/* FT_PRINTF_TEST("%.*1$f", 2., -5); */
	/* FT_PRINTF_TEST("%.*1$g", 2., 5); */
	/* FT_PRINTF_TEST("%.*1$e", 2., 5); */
	/* FT_PRINTF_TEST("%.*1$a", 2., 5); */
	/* FT_PRINTF_TEST("%.*1$f", 2., 5); */

	FT_PRINTF_TEST("%20.*2$g", 0.01, -5);
	FT_PRINTF_TEST("%20.*2$e", 0.01, -5);
	FT_PRINTF_TEST("%20.*2$a", 0.01, -5);
	FT_PRINTF_TEST("%20.*2$f", 0.01, -5);

	FT_PRINTF_TEST("%.*2$g", 0.01, -5);
	FT_PRINTF_TEST("%.*2$e", 0.01, -5);
	FT_PRINTF_TEST("%.*2$a", 0.01, -5);
	FT_PRINTF_TEST("%.*2$f", 0.01, -5);
	#endif /* TDOL TFLOAT */
	#endif /* TFLOAT */

	#if TDIBOUX == 1
	FT_PRINTF_TEST("----------------------------------------------\n");
	FT_PRINTF_TEST("%d %% %o %% %u %x %X", 123, -1, -1, -1, -1);
	FT_PRINTF_TEST("%d %% %o %% %u %x %X", 123, 0, 0, 0, 0);
	FT_PRINTF_TEST("%d %#o %u %#x %#X", 123, -1, -1, -1, -1);
	FT_PRINTF_TEST("%d %#o %u %#x %#X", 123, 0, 0, 0, 0);
	#ifndef __linux__
	FT_PRINTF_TEST("%D %O %U %X", -1L, -1L, -1L, -1L);
	#endif
	FT_PRINTF_TEST("%ld %lo %lu %lx", -1L, -1L, -1L, -1L);
	FT_PRINTF_TEST("%d %d %d %d %d %d", 123, 0, -123, -1, INT_MAX, INT_MIN);
	FT_PRINTF_TEST("%i %i %i %i %i %i", 123, 0, -123, -1, INT_MAX, INT_MIN);
	FT_PRINTF_TEST("%li %li %li %li %li %li", 123L, 0L, -123L, -1L, LONG_MAX, LONG_MIN);
	FT_PRINTF_TEST("%ld %ld %ld %ld", 123L, 0L, -123L, -1L);
	FT_PRINTF_TEST("%lld %lld %lld %lld", 123LL, 0LL, -123LL, -1LL);
	FT_PRINTF_TEST("%ld %ld", ULONG_MAX, LONG_MAX);
	FT_PRINTF_TEST("%hd %hi", (short)65535, (short)65535);
	FT_PRINTF_TEST("%hd %hi", (short)-1, (short)-1);
	FT_PRINTF_TEST("%hhd %hhi", (char)-1, (char)-1);
	FT_PRINTF_TEST("%hhd %hhi", (char)65535, (char)65535);
	FT_PRINTF_TEST("%ld %li", (long)-1, (long)-1);
	FT_PRINTF_TEST("%lld %lli", (long long)-1, (long long)-1);
	#ifndef __linux__
	FT_PRINTF_TEST("%D", (long)-1);
	FT_PRINTF_TEST("%D", LONG_MAX);
	#endif
	FT_PRINTF_TEST("%hhd %hhd %hhd", (char)-1, (char)255, (char)'9');

	FT_PRINTF_TEST("%-+10.5d", 4242);
	FT_PRINTF_TEST("%+10.5d", 4242);
	FT_PRINTF_TEST("% 10.5d", 4242);
	FT_PRINTF_TEST("%-.5d", 4242);
	FT_PRINTF_TEST("%+.5d", 4242);
	FT_PRINTF_TEST("%20+10d%d", 5, 10);
	FT_PRINTF_TEST("%20+10d|%d", 5, 10);
	FT_PRINTF_TEST("%20 10d|%d", 5, 10);
	FT_PRINTF_TEST("%20-10d|%d", 5, 10);
	FT_PRINTF_TEST("%20#10d|%d", 5, 10);
	FT_PRINTF_TEST("% 033.1d", 1234567890123);
	FT_PRINTF_TEST("%# 033.0d", 1234567890123);
	FT_PRINTF_TEST("%# 033.1d", 1234567890123);
	FT_PRINTF_TEST("%# 033.0d", 1234567890123);
	FT_PRINTF_TEST("%#33.0d", 1234567890123);
	FT_PRINTF_TEST("%#.0d", 1234567890123);
	FT_PRINTF_TEST("%#d", 1234567890123);
	FT_PRINTF_TEST("----------------------------------------------\n");
	FT_PRINTF_TEST("%zd %zi", (size_t)-1, (size_t)-1);
	FT_PRINTF_TEST("%jd %zi", (intmax_t)-1, (intmax_t)-1);
	FT_PRINTF_TEST("%jd %zi", (uintmax_t)-1, (uintmax_t)-1);
	FT_PRINTF_TEST("%jd %zi", (uintmax_t)123456789, (uintmax_t)123456789);
	FT_PRINTF_TEST("%td %td", -1, UINT_MAX);
	FT_PRINTF_TEST("%td %td", LONG_MAX, ULONG_MAX);
	FT_PRINTF_TEST("%.2d|%10d|%2d|%10.2d|%-10d|%-10.2d|%10.d", 123456, 123456, 123456, 123456, 123456, 123456, 123456);
	FT_PRINTF_TEST("%010d|%02d|%010.2d|%010.d|% 10d|% 10.2d|%- 10.2d", 123456, 123456, 123456, 123456, 123456, 123456, 123456);
	FT_PRINTF_TEST("%+010d|%+02d|%+010.2d|%+010.d|%+10d|%+10.2d|%+-10.2d", 123456, 123456, 123456, 123456, 123456, 123456, 123456);
	FT_PRINTF_TEST("% 010d|% 02d|% 010.2d|% 010.d|% 10d|% 10.2d|% -10.2d", 123456, 123456, 123456, 123456, 123456, 123456, 123456);
	FT_PRINTF_TEST("%%5d  |%%-5d |%%+5d |%%+-5d|%% 5d |%%05d |%%5.0d|%%5.2d|%%d|%%+-d");
	FT_PRINTF_TEST("%5d|%-5d|%+5d|%+-5d|% 5d|%05d|%5.0d|%5.2d|%d|%-+d", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	FT_PRINTF_TEST("%5d|%-5d|%+5d|%+-5d|% 5d|%05d|%5.0d|%5.2d|%d|%-+d", 1, 1, 1, 1, 1, 1, 1, 1, 1, 0);
	FT_PRINTF_TEST("%5d|%-5d|%+5d|%+-5d|% 5d|%05d|%5.0d|%5.2d|%d|%-+d", -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
	FT_PRINTF_TEST("%5d|%-5d|%+5d|%+-5d|% 5d|%05d|%5.0d|%5.2d|%d|+%-+d", 100000, 100000, 100000, 100000, 100000, 100000, 100000, 100000, 100000, 100000);
	FT_PRINTF_TEST("% i", -55);

	FT_PRINTF_TEST("%5id", 55);
	FT_PRINTF_TEST("%ld", (long int)10);
	FT_PRINTF_TEST("%-50.10d", 10);
	FT_PRINTF_TEST("test + |%+d", 1234567);
	FT_PRINTF_TEST("test witdh |%050d", 1234567);
	FT_PRINTF_TEST("test |%0d", 67);
	FT_PRINTF_TEST("test |%lld", (unsigned long long int)-10);
	FT_PRINTF_TEST("%  +d", -42);
	FT_PRINTF_TEST("%-5+d", -42);
	FT_PRINTF_TEST("%hd", "4294967296");	
	FT_PRINTF_TEST("%zd", "4294967296");	
	FT_PRINTF_TEST("%hd", -1);	
	FT_PRINTF_TEST("%hu", -1);	
	FT_PRINTF_TEST("%zd", -1);	
	
	FT_PRINTF_TEST("%zhd %zhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%zhhd %zhhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%zld %zld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%zlld %zlld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%zLd %zLd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%zjd %zjd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%zzd %zzd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%ztd %ztd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%jhd %jhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%jhhd %jhhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%jld %jld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%jlld %jlld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%jLd %jLd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%jjd %jjd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%jzd %jzd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%jtd %jtd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%thd %thd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%thhd %thhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%tld %tld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%tlld %tlld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%tLd %tLd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%tjd %tjd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%tzd %tzd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%ttd %ttd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%Lld %Lld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%Llld %Llld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%Ljd %Ljd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%Lzd %Lzd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%Ltd %Ltd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%llhd %llhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%llhhd %llhhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%llld %llld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%lllld %lllld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%llLd %llLd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%lljd %lljd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%llzd %llzd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%lltd %lltd", -1, ULLONG_MAX);
	
	FT_PRINTF_TEST("%lhd %lhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%lhhd %lhhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%lld %lld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%llld %llld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%lLd %lLd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%ljd %ljd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%lzd %lzd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%ltd %ltd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhhd %hhhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhhhd %hhhhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhld %hhld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhlld %hhlld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhjd %hhjd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhzd %hhzd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhtd %hhtd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhd %hhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhhd %hhhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hld %hld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hlld %hlld", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hjd %hjd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hzd %hzd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%htd %htd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%zhd %zhd", (long)INT_MAX + 1, (long)UINT_MAX + 1);

	FT_PRINTF_TEST("%  +d", -42);
	FT_PRINTF_TEST("%20+10d", 5);
	FT_PRINTF_TEST("%-5+d|%+-5d", -42, 42);
	FT_PRINTF_TEST("%+-5d|%+-5d", -42, 42);
	FT_PRINTF_TEST("%5d|%-5d|%+5d|%+-5d|% 5d|%05d|%5.0d|%5.2d|%d|%-+d", -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
	FT_PRINTF_TEST("%5.2d", -1);
	FT_PRINTF_TEST("%+d", -42);
	FT_PRINTF_TEST("%++d", -42);
	FT_PRINTF_TEST("%5+d", -42);
	FT_PRINTF_TEST("%5--d", -42);
	FT_PRINTF_TEST("%50-.10d", 10);
	FT_PRINTF_TEST("%50-d", 10);
	FT_PRINTF_TEST("%50+d", 10);

	FT_PRINTF_TEST("----------------------------------------------\n");
	FT_PRINTF_TEST("%lu %lu %lu %lu", 123L, 0L, -123L, -1L);
	FT_PRINTF_TEST("%llu %llu %llu %llu", 123LL, 0LL, -123LL, -1LL);
	FT_PRINTF_TEST("%llo %llo %llo %llo", 123LL, 0LL, -123LL, -1LL);
	FT_PRINTF_TEST("%llx %llx %llx %llx", 123LL, 0LL, -123LL, -1LL);
	FT_PRINTF_TEST("%lu %lu", ULONG_MAX, LONG_MAX);
	FT_PRINTF_TEST("%#lx %#lx", ULONG_MAX, LONG_MAX);
	FT_PRINTF_TEST("%#x %#x", ULONG_MAX, LONG_MAX);
	FT_PRINTF_TEST("%hu %ho %hx %hX", (short)65535, (short)65535, (short)65535, (short)65535);
	FT_PRINTF_TEST("%hu %ho %hx %hX", (short)-1, (short)-1, (short)-1, (short)-1);
	FT_PRINTF_TEST("%hhu %hho %hhx %hhX", (char)-1, (char)-1, (char)-1, (char)-1);
	FT_PRINTF_TEST("%hhu %hho %hhx %hhX", (char)65535, (char)65535, (char)65535, (char)65535);
	FT_PRINTF_TEST("%lu %lo %lx %lX", (long)-1, (long)-1, (long)-1, (long)-1);
	FT_PRINTF_TEST("%llu %llo %llx %llX", (long long)-1, (long long)-1, (long long)-1, (long long)-1);
	#ifndef __linux__
	FT_PRINTF_TEST("%O %U %X", (long)-1, (long)-1, (long)-1);
	FT_PRINTF_TEST("%U", LONG_MAX);
	#endif
	FT_PRINTF_TEST("%hhu %hhu %hhu", (char)-1, (char)255, (char)'9');
	FT_PRINTF_TEST("----------------------------------------------\n");
	FT_PRINTF_TEST("%zo %zu %zx", (size_t)-1, (size_t)-1, (size_t)-1);
	FT_PRINTF_TEST("%jo %ju %jx", (intmax_t)-1, (intmax_t)-1, (intmax_t)-1);
	FT_PRINTF_TEST("%jo %ju %jx", (uintmax_t)-1, (uintmax_t)-1, (uintmax_t)-1);
	FT_PRINTF_TEST("%jo %ju %jx", (uintmax_t)123456789, (uintmax_t)123456789, (uintmax_t)123456789);

	char *ptr = "132";
	FT_PRINTF_TEST("%p %p", NULL, ptr);
	FT_PRINTF_TEST("%p", 0xffffffff + 1);
	char *ptr2;
	ptr2 = ptr + 2;
	FT_PRINTF_TEST("%td %td", ptr - ptr2, ptr2 - ptr);
	FT_PRINTF_TEST("%to %tu %tx", ptr - ptr2, ptr2 - ptr, ptr - ptr2);
	/* FT_PRINTF_TEST("%b %b %b %b %b %b %b %b %b", 123, 1, 2, 3, 4, 5, 6,7,8); */
	/* FT_PRINTF_TEST("%#b %#b %#b %#b %#b %#b %#b %#b", 1, 2, 3, 4, 5, 6,7,8); */
	/* FT_PRINTF_TEST("%B", -1L) */
	/* FT_PRINTF_TEST("%jb %hb %hhb %lb %llb", (intmax_t)-1, (intmax_t)-1, (intmax_t)-1, (intmax_t)-1, (intmax_t)-1); */
	/* FT_PRINTF_TEST("%jb %hb %hhb %lb %llb", (uintmax_t)-1, (uintmax_t)-1, (uintmax_t)-1, (uintmax_t)-1, (uintmax_t)-1); */
	/* FT_PRINTF_TEST("%jb %hb %hhb %lb %llb", (uintmax_t)123456789, (uintmax_t)123456789, (uintmax_t)123456789, (uintmax_t)123456789, (uintmax_t)123456789); */
	/* FT_PRINTF_TEST("%B  %zb", (long)-1, (size_t)-1); */
	/* FT_PRINTF_TEST("%oB %#B", 1, 2); */
	/* FT_PRINTF_TEST("%b %b", (wchar_t)(L'¡'), (wchar_t)(L'¢')); */
	/* FT_PRINTF_TEST("%b %b", (wchar_t)(L'¡'), (wchar_t)(L'¢')); */
	/* FT_PRINTF_TEST("%b", INT_MAX); */
	/* FT_PRINTF_TEST("%b", UINT_MAX); */
	/* FT_PRINTF_TEST("%lb", LONG_MAX); */
	/* FT_PRINTF_TEST("%lb", ULONG_MAX); */
	FT_PRINTF_TEST("%#.15o %#.15x %#.15X", 132, 123, 123);
	FT_PRINTF_TEST("%#10.15o %#10.15x %#10.15X", 132, 123, 123);
	FT_PRINTF_TEST("test # |%#x", 10);
	FT_PRINTF_TEST("test # |%#xd", 10);
	FT_PRINTF_TEST("test # |%0#x", 10);
	FT_PRINTF_TEST("test # |%#010x", 10);


	FT_PRINTF_TEST("%#-08x", 42);
	FT_PRINTF_TEST("%-#6o", 2500);
	FT_PRINTF_TEST("%-5.10o", 2500);
	FT_PRINTF_TEST("@moulitest: %#.x, %#.0x", 0, 0);
	FT_PRINTF_TEST("@moulitest: %#.o, %#.0o", 0, 0);
	FT_PRINTF_TEST("@moulitest: %.o, %.0o", 0, 0);
	FT_PRINTF_TEST("% u", 123);
	FT_PRINTF_TEST("%zhd %zhd", -1, ULLONG_MAX); //doublon
	FT_PRINTF_TEST("% hhhllllllLqjzttzjqLllllllhhhd", UINT_MAX);//doublon
	FT_PRINTF_TEST("% hhhllllllLqjzttzjqLllllllhhhd", (long)UINT_MAX + 1);
	FT_PRINTF_TEST("% hhhlllLqjzttzjqLllllllhhhd", UINT_MAX);
	FT_PRINTF_TEST("% hhhlllqjzttzjqlllhhh", UINT_MAX);
	FT_PRINTF_TEST("% hhhlllqjzttzjqlllhhhd", UINT_MAX);
	FT_PRINTF_TEST("%hhd %hhu", CHAR_MAX, UCHAR_MAX);
	FT_PRINTF_TEST("%hd %hu", SHRT_MAX, USHRT_MAX);
	FT_PRINTF_TEST("%hd %hu", (long)SHRT_MAX + 1, (long)USHRT_MAX + 1);
	FT_PRINTF_TEST("%d %u", INT_MAX, UINT_MAX);
	FT_PRINTF_TEST("%ld %lu", LONG_MAX, ULONG_MAX);
	FT_PRINTF_TEST("%lld %llu", LLONG_MAX, ULLONG_MAX);
	FT_PRINTF_TEST("%Ld %Lu", INT_MAX, UINT_MAX);
	FT_PRINTF_TEST("%lLd %lLu", LONG_MAX, ULONG_MAX);
	FT_PRINTF_TEST("%lLd %lLu", (long double)LONG_MAX + 1, ULONG_MAX + 1);

	FT_PRINTF_TEST("%lLd %lLu", LLONG_MAX, ULLONG_MAX);
	FT_PRINTF_TEST("%lLd %lLu", INT_MAX, UINT_MAX);
	FT_PRINTF_TEST("%lLd %lLu", (long)INT_MAX + 1, (long)UINT_MAX + 1);

	FT_PRINTF_TEST("%zd %zu %zo %zx", (size_t)-1, (ssize_t)-1, (ssize_t)-1, (ssize_t)-1);
	FT_PRINTF_TEST("%#zd %#zu %#zo %#zx", (size_t)-1, (ssize_t)-1, (ssize_t)-1, (ssize_t)-1);
	FT_PRINTF_TEST("%td", (ptrdiff_t)-1);
	FT_PRINTF_TEST("%jd %ju %jo %jx", (intmax_t)-1, (uintmax_t)-1, (uintmax_t)-1, (uintmax_t)-1);
	FT_PRINTF_TEST("%#jd %#ju %#jo %#jx", (intmax_t)-1, (uintmax_t)-1, (uintmax_t)-1, (uintmax_t)-1);
	FT_PRINTF_TEST("%# o", 1);
	FT_PRINTF_TEST("%#.15o %#.15x %#.15X", 132, 123, 123);
	FT_PRINTF_TEST("%llh", UINT_MAX);
	FT_PRINTF_TEST("%hlu", UINT_MAX);
	FT_PRINTF_TEST("%hllu", UINT_MAX);
	FT_PRINTF_TEST("%llhu", UINT_MAX);
	FT_PRINTF_TEST("%hlu", UINT_MAX);
	FT_PRINTF_TEST("%llh ", UINT_MAX);

	#ifndef __linux__
	FT_PRINTF_TEST("%hU", UINT_MAX);
	FT_PRINTF_TEST("%hhU", UINT_MAX);
	FT_PRINTF_TEST("%hD", UINT_MAX);
	FT_PRINTF_TEST("%llD", UINT_MAX);
	FT_PRINTF_TEST("%lD", UINT_MAX);
	#endif
	
	FT_PRINTF_TEST("%hhd %hhu %hho %hhx %hhX", (char)-1, (char)-1, (char)-1, (char)-1, (char)-1);
	FT_PRINTF_TEST("%hhd %hhu %hho %hhx %hhX", (char)65535, (char)65535, (char)65535, (char)65535, (char)65535);


	FT_PRINTF_TEST("%lld %llu", LLONG_MAX, ULLONG_MAX);
	FT_PRINTF_TEST("%lld %llu", (long)INT_MAX + 1, (long)UINT_MAX + 1);

	FT_PRINTF_TEST("%Ld %Lu", LLONG_MAX, ULLONG_MAX);
	FT_PRINTF_TEST("%Ld %Lu", (long)INT_MAX + 1, (long)UINT_MAX + 1);
	FT_PRINTF_TEST("%ld", (long double)ULONG_MAX + 1);
	FT_PRINTF_TEST("%ld", (long double)LONG_MAX + 1);
	FT_PRINTF_TEST("%Ld", (long double)LONG_MAX + 1);
	FT_PRINTF_TEST("%Ld %Lu", (long)INT_MAX + 1, (long)UINT_MAX + 1);
	FT_PRINTF_TEST("%Ld %Lu", -1, -1);
	FT_PRINTF_TEST("%Ld %Lu", LLONG_MAX, ULLONG_MAX);

	FT_PRINTF_TEST("%lld %llu", (long)INT_MAX + 1, (long)UINT_MAX + 1);
	FT_PRINTF_TEST("%Ld %Lu", INT_MAX, UINT_MAX);

	FT_PRINTF_TEST("%hhu", (char)65535);
	FT_PRINTF_TEST("%10d", 5);
	FT_PRINTF_TEST("%10.7d", 5);
	FT_PRINTF_TEST("%10.7d", -5);
	FT_PRINTF_TEST("%010d", 5);
	FT_PRINTF_TEST("%010.7d", 5);
	FT_PRINTF_TEST("%0020d", 10);

	FT_PRINTF_TEST("%zhd", "4294967296");
	FT_PRINTF_TEST("%Lhd %Lhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%Lhhd %Lhhd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%LLd %LLd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hhLd %hhLd", -1, ULLONG_MAX);
	FT_PRINTF_TEST("%hLd %hLd", -1, ULLONG_MAX);
	
	FT_PRINTF_TEST("%hLd", -1);


	FT_PRINTF_TEST("{%05p}", 0);
	FT_PRINTF_TEST("%.0p, %.p", 0, 0);
	FT_PRINTF_TEST("%.0p, %.p", NULL, NULL);
	FT_PRINTF_TEST("%#.0x, %#.x", 0, 0);
	FT_PRINTF_TEST("%.5p", 0);
	FT_PRINTF_TEST("%9.2p", 1234);
	FT_PRINTF_TEST("%9.2p", 1234567);
	FT_PRINTF_TEST("%2.9p", 1234);
	FT_PRINTF_TEST("%2.9p", 1234567);
	FT_PRINTF_TEST("%lp %lp %lp", 42, -1 , ULONG_MAX);
	FT_PRINTF_TEST("%.p, %.0p", 0, 0);
	FT_PRINTF_TEST("%p %p", 0, 123);
	FT_PRINTF_TEST("%33 10d", 123);
	FT_PRINTF_TEST("%p %#x", -1, -1);
	FT_PRINTF_TEST("%p", ULONG_MAX);
	FT_PRINTF_TEST("%p %p", ULONG_MAX, LONG_MAX);
	FT_PRINTF_TEST("%p", "void *");
	FT_PRINTF_TEST("% p|%+p", 42, 42);
	#endif /* TDIBOUX */

	#if TSC == 1
	# if SETLOCAL_ENABLE != 0
	setlocale(LC_ALL, "fr_FR.UTF-8");
	# endif
	FT_PRINTF_TEST("----------------------------------------------\n");
	FT_PRINTF_TEST("%S %ls", (wchar_t*)(L"Wide String"), (wchar_t*)(L"Wide String"));
	FT_PRINTF_TEST("%C %lc %c", (wchar_t)(L'W'), (wchar_t)(L'T'), 'A');
	FT_PRINTF_TEST("%ls %ls", (wchar_t *)(L"¡"), (wchar_t *)(L"¢"));
	FT_PRINTF_TEST("%lc %lc", (wchar_t)(L'\xA1'), (wchar_t)(L'\xA2'));
	FT_PRINTF_TEST("%ls %ls", (wchar_t *)(L"\xA1"), (wchar_t *)(L"\xA2"));
	FT_PRINTF_TEST("%lc %lc", (wchar_t)(L'¡'), (wchar_t)(L'¢'));
	FT_PRINTF_TEST("%lc %ls", (wchar_t)(L'猫'), (wchar_t *)(L"Ë©≤"));
	FT_PRINTF_TEST("test |%ls", (wchar_t*)(L"Wide String"));
	FT_PRINTF_TEST("%S", (wchar_t *)(L"猫"));
	FT_PRINTF_TEST("%C %C", (wchar_t)(L'猫'), (wchar_t)0);
	FT_PRINTF_TEST("%C", L'\u732b');
	FT_PRINTF_TEST("%ls", (wchar_t *)(L"\x10330"));
	FT_PRINTF_TEST("%lc", (wchar_t)(L'\u10330'));
	FT_PRINTF_TEST("%lc|%d|%ls", L'\u732b', (wchar_t)(L'猫'), (wchar_t *)(L"Ë©≤"));
	FT_PRINTF_TEST("%lc", (wchar_t)(L'\u10B4'));

	FT_PRINTF_TEST("%hhC", (wchar_t)(L'©'));
	FT_PRINTF_TEST("%hC", (wchar_t)(L'©'));
	FT_PRINTF_TEST("%lC", (wchar_t)(L'©'));
	FT_PRINTF_TEST("%llC", (wchar_t)(L'©'));
	FT_PRINTF_TEST("%hhS", (wchar_t *)(L"©"));
	FT_PRINTF_TEST("%hS", (wchar_t *)(L"©"));
	FT_PRINTF_TEST("%lS", (wchar_t *)(L"©"));
	FT_PRINTF_TEST("%llS", (wchar_t *)(L"©"));
	FT_PRINTF_TEST("%hhC", (wchar_t)(L'猫'));
	FT_PRINTF_TEST("%hC", (wchar_t)(L'猫'));
	FT_PRINTF_TEST("%lC", (wchar_t)(L'猫'));
	FT_PRINTF_TEST("%llC", (wchar_t)(L'猫'));
	FT_PRINTF_TEST("%hhS", (wchar_t *)(L"猫"));
	FT_PRINTF_TEST("%hS", (wchar_t *)(L"猫"));
	FT_PRINTF_TEST("%lS", (wchar_t *)(L"猫"));
	FT_PRINTF_TEST("%llS", (wchar_t *)(L"猫"));
	FT_PRINTF_TEST("%lc", (wchar_t)(65537));

	FT_PRINTF_TEST("%lc %lc %lc", L'Α', L'Β', L'Γ');
	FT_PRINTF_TEST("%lc %lc %lc", L'α', L'β', L'γ');
	FT_PRINTF_TEST("%lc %lc %lc", L'Δ', L'Ε', L'Ζ');
	FT_PRINTF_TEST("%lc %lc %lc", L'δ', L'ε', L'ζ');
	FT_PRINTF_TEST("%lc %lc %lc", L'Η', L'Θ', L'Ι');
	FT_PRINTF_TEST("%lc %lc %lc", L'η', L'θ', L'ι');
	FT_PRINTF_TEST("%lc %lc %lc", L'Κ', L'Λ', L'Μ');
	FT_PRINTF_TEST("%lc %lc %lc", L'κ', L'λ', L'μ');
	FT_PRINTF_TEST("%lc %lc %lc", L'Ν', L'Ξ', L'Ο');
	FT_PRINTF_TEST("%lc %lc %lc", L'ν', L'ξ', L'ο');
	FT_PRINTF_TEST("%lc %lc %lc", L'Π', L'Ρ', L'Σ');
	FT_PRINTF_TEST("%lc %lc %lc", L'π', L'ρ', L'σ');
	FT_PRINTF_TEST("%lc %lc %lc", L'Τ', L'Υ', L'Φ');
	FT_PRINTF_TEST("%lc %lc %lc", L'τ', L'υ', L'φ');
	FT_PRINTF_TEST("%lc %lc %lc", L'Χ', L'Ψ', L'Ω');
	FT_PRINTF_TEST("%lc %lc %lc", L'χ', L'ψ', L'ω');
	FT_PRINTF_TEST("%lc", L'Α');
	FT_PRINTF_TEST("%lc", L'ω');

	FT_PRINTF_TEST("----------------------------------------------\n");
	FT_PRINTF_TEST("%lc|%d|%ls", (wchar_t)(L'猫'), (wchar_t)(L'猫'), (wchar_t *)(L"Ë©≤"));
	FT_PRINTF_TEST("%.2s|%10s|%2s|%10.2s|%-10s|%-10.2s|%10.s", "azerty", "azerty", "azerty", "azerty", "azerty", "azerty", "azerty");
	FT_PRINTF_TEST("%62s", "azerty");
	FT_PRINTF_TEST("%3s%-6s", "no", "where");
	FT_PRINTF_TEST("test . |%10.2s|%20.10s|%-20.10s", "qwerty", "qwerty", "qwerty");
	FT_PRINTF_TEST("%10.2s", "qwerty");
	char *c = NULL;
	# if SETLOCAL_ENABLE != 0
	c = setlocale(LC_ALL, "fr_FR.UTF-8");
	# endif
	FT_PRINTF_TEST("%C|%s", (wchar_t)(L'é'), c);
	FT_PRINTF_TEST("%C|%s", (wchar_t)(L'猫'), c);
	# if SETLOCAL_ENABLE != 0
	c = setlocale(LC_ALL, "fr_FR.iso885915@euro");
	# endif
	FT_PRINTF_TEST("%C|%s", (wchar_t)(L'é'), c);
	FT_PRINTF_TEST("%C|%s", (wchar_t)(L'猫'), c);
	FT_PRINTF_TEST("%ls %ls", (wchar_t *)(L"¡"), (wchar_t *)(L"¢"));
	FT_PRINTF_TEST("%lc %lc", (wchar_t)(L'\xA1'), (wchar_t)(L'\xA2'));
	FT_PRINTF_TEST("%ls %ls", (wchar_t *)(L"\xA1"), (wchar_t *)(L"\xA2"));
	FT_PRINTF_TEST("test |%ls", (wchar_t*)(L"Wide String"));
	FT_PRINTF_TEST("%lc %lc", (wchar_t)(L'¡'), (wchar_t)(L'¢'));
	FT_PRINTF_TEST("%lc %ls", (wchar_t)(L'猫'), (wchar_t *)(L"Ë©≤"));
	FT_PRINTF_TEST("%lc", (wchar_t)(L'©'));
	FT_PRINTF_TEST("%C", (wchar_t)(L'猫'));

	FT_PRINTF_TEST("%hhC", (wchar_t)(L'©'));
	FT_PRINTF_TEST("%hC", (wchar_t)(L'©'));
	FT_PRINTF_TEST("%lC", (wchar_t)(L'©'));
	FT_PRINTF_TEST("%llC", (wchar_t)(L'©'));
	FT_PRINTF_TEST("%hhS", (wchar_t *)(L"©"));
	FT_PRINTF_TEST("%hS", (wchar_t *)(L"©"));
	FT_PRINTF_TEST("%lS", (wchar_t *)(L"©"));
	FT_PRINTF_TEST("%llS", (wchar_t *)(L"©"));
	FT_PRINTF_TEST("%hhC", (wchar_t)(L'猫'));
	FT_PRINTF_TEST("%hC", (wchar_t)(L'猫'));
	FT_PRINTF_TEST("%lC", (wchar_t)(L'猫'));
	FT_PRINTF_TEST("%llC", (wchar_t)(L'猫'));
	FT_PRINTF_TEST("%hhS", (wchar_t *)(L"猫"));
	FT_PRINTF_TEST("%hS", (wchar_t *)(L"猫"));
	FT_PRINTF_TEST("%lS", (wchar_t *)(L"猫"));
	FT_PRINTF_TEST("%llS", (wchar_t *)(L"猫"));
	FT_PRINTF_TEST("%lc", (wchar_t)(65537));

	FT_PRINTF_TEST("%ls %ls", (wchar_t *)(L"\xC2\xA1"), (wchar_t *)(L"\xE2\xA2"));
	FT_PRINTF_TEST("%s %s", (char *)("\xC2\xA1"), (wchar_t *)(L"\xE2\xA2"));
	# if SETLOCAL_ENABLE != 0
	setlocale(LC_ALL, "fr_FR.UTF-8");
	# endif
	FT_PRINTF_TEST("%ls %ls", (wchar_t *)(L"\xC2\xA1"), (wchar_t *)(L"\xE2\xA2"));
	FT_PRINTF_TEST("%s %s", (char *)("\xC2\xA1"), (wchar_t *)(L"\xE2\xA2"));
	# if SETLOCAL_ENABLE != 0
	setlocale(LC_ALL, "");
	# endif

	FT_PRINTF_TEST("%.2s is a string", "");
	FT_PRINTF_TEST("%2c", 0);
	FT_PRINTF_TEST("%2c", 0);
	FT_PRINTF_TEST("null %c and text", 0);
	FT_PRINTF_TEST("% c", 0);
	FT_PRINTF_TEST("%.2c", NULL);
	FT_PRINTF_TEST("{% S}", NULL);
	FT_PRINTF_TEST("%.0c", NULL);
	FT_PRINTF_TEST("111%s333%s555", "222", "444");
	FT_PRINTF_TEST("{%05.s}", 0);
	FT_PRINTF_TEST("%010s is a string", "this");
	FT_PRINTF_TEST("%@moulitest: %c", 0);



	FT_PRINTF_TEST("%.2S", L"");
	FT_PRINTF_TEST("%.2s", "");
	FT_PRINTF_TEST("%0.2S", L"");
	FT_PRINTF_TEST("%0.2s", "");
	FT_PRINTF_TEST("%0.10S", L"");
	FT_PRINTF_TEST("%0.10s", "");
	FT_PRINTF_TEST("%0.S", L"");
	FT_PRINTF_TEST("%0.s", "");
	FT_PRINTF_TEST("%010S", L"");
	FT_PRINTF_TEST("%010s", "");
	FT_PRINTF_TEST("%010C", L'\0');
	FT_PRINTF_TEST("%010c", 0);
	FT_PRINTF_TEST("%s %C %d %p %x %% %S", "bonjour ", L'該', 42, &free, 42, L"ريزنخ محل");
	FT_PRINTF_TEST("%C", L'δ');
	FT_PRINTF_TEST("%C", L'ي');
	FT_PRINTF_TEST("%lc, %lc", L'暖', L'ح');
	FT_PRINTF_TEST("%ls, %ls", L"暖炉", L"ريزنخ محل");
	FT_PRINTF_TEST("%S %S %S", L"Α α", L"Β β", L"Γ γ");
	FT_PRINTF_TEST("%S %S %S", L"Δ δ", L"Ε ε", L"Ζ ζ");
	FT_PRINTF_TEST("%S %S %S", L"Η η", L"Θ θ", L"Ι ι");
	FT_PRINTF_TEST("%S %S %S", L"Κ κ", L"Λ λ", L"Μ μ");
	FT_PRINTF_TEST("%S %S %S", L"Ν ν", L"Ξ ξ", L"Ο ο");
	FT_PRINTF_TEST("%S %S %S", L"Π π", L"Ρ ρ", L"Σ σ");
	FT_PRINTF_TEST("%S %S %S", L"Τ τ", L"Υ υ", L"Φ φ");
	FT_PRINTF_TEST("%S %S %S %S", L"Χ χ", L"Ψ ψ", L"Ω ω", L"");
	FT_PRINTF_TEST("%.4S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.4S", L"我是一只猫。");


	FT_PRINTF_TEST("%.2S", L"。");
	FT_PRINTF_TEST("%.3S", L"。");
	FT_PRINTF_TEST("%S", L"我是一只猫。");
	FT_PRINTF_TEST("%.4S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.4S", L"我是一只猫。");
	FT_PRINTF_TEST("%.5S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.5S", L"我是一只猫。");
	FT_PRINTF_TEST("%.6S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.6S", L"我是一只猫。");
	FT_PRINTF_TEST("%.7S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.7S", L"我是一只猫。");
	FT_PRINTF_TEST("%.8S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.8S", L"我是一只猫。");
	FT_PRINTF_TEST("%.9S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.9S", L"我是一只猫。");
	FT_PRINTF_TEST("%.10S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.10S", L"我是一只猫。");
	FT_PRINTF_TEST("%.11S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.11S", L"我是一只猫。");
	FT_PRINTF_TEST("%.7S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.7S", L"我是一只猫。");
	FT_PRINTF_TEST("%.12S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.12S", L"我是一只猫。");
	FT_PRINTF_TEST("%.13S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.13S", L"我是一只猫。");
	FT_PRINTF_TEST("%.14S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.14S", L"我是一只猫。");
	FT_PRINTF_TEST("%.15S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.15S", L"我是一只猫。");
	FT_PRINTF_TEST("%.16S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.16S", L"我是一只猫。");
	FT_PRINTF_TEST("%.17S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.17S", L"我是一只猫。");
	FT_PRINTF_TEST("%.18S", L"我是一只猫。");
	FT_PRINTF_TEST("%15.18S", L"我是一只猫。");
	
	FT_PRINTF_TEST("%02s", 0);
	FT_PRINTF_TEST("%020s", 0);
	FT_PRINTF_TEST("%2s", 0);
	FT_PRINTF_TEST("%5s", 0);
	FT_PRINTF_TEST("%15s", 0);
	FT_PRINTF_TEST("{%05.10s}", 0);
	FT_PRINTF_TEST("%15S", 0);
	#endif /* TSC */

	#if TETOI == 1
	FT_PRINTF_TEST("----------------------------------------------\n");
	FT_PRINTF_TEST("%*.*d", 5, 10, 20);
	FT_PRINTF_TEST("test . |%.*d", 4, 10);
	FT_PRINTF_TEST("test . |%.*d|%d", 4, 10, 15);
	FT_PRINTF_TEST("test . |%d|%.*d|%d", 2, 4, 10, 15);

	FT_PRINTF_TEST("{%*d}", -5, 42);
	FT_PRINTF_TEST("{%*c}", -15, 0);
	FT_PRINTF_TEST("{%0*d}", -15, 0);
	FT_PRINTF_TEST("{%0*d}", 15, 0);
	FT_PRINTF_TEST("%.*p", 0, 0);
	FT_PRINTF_TEST("%*d", 5, 0);
	FT_PRINTF_TEST("%*3d", 5, 0);
	FT_PRINTF_TEST("%*3d", 15, 1);
	FT_PRINTF_TEST("%*0d", 15, 1);

	FT_PRINTF_TEST("%05.*d", -15, 42);
	FT_PRINTF_TEST("%05.*3d", 15, 42);
	FT_PRINTF_TEST("%05.*0d", 15, 42);
	FT_PRINTF_TEST("%05.*0d", -15, 42);
	FT_PRINTF_TEST("%05.*30d", 0, 42);
	FT_PRINTF_TEST("%05.*d", 0, 42);
	FT_PRINTF_TEST("%05.*3d", 0, 42);
	FT_PRINTF_TEST("%05.*3d", -15, 42);
	FT_PRINTF_TEST("%05.*1d", 15, 42);
	FT_PRINTF_TEST("%05.*1d", 0, 42);
	FT_PRINTF_TEST("%05.*1d", -15, 42);
	FT_PRINTF_TEST("%05*3d", -15, 42);
	FT_PRINTF_TEST("%05*3d", 0, 42);
	FT_PRINTF_TEST("%05*3d", 15, 42);
	FT_PRINTF_TEST("%5*3d", -15, 42);
	FT_PRINTF_TEST("%5*3d", 0, 42);
	FT_PRINTF_TEST("%5*3d", 15, 42);
	FT_PRINTF_TEST("%0*3d", -15, 42);
	FT_PRINTF_TEST("%0*3d", 0, 42);
	FT_PRINTF_TEST("%0*3d", 15, 42);
	FT_PRINTF_TEST("%05*1d", -15, 42);
	FT_PRINTF_TEST("%05*1d", 0, 42);
	FT_PRINTF_TEST("%05*1d", 15, 42);
	FT_PRINTF_TEST("%5*1d", -15, 42);
	FT_PRINTF_TEST("%5*1d", 0, 42);
	FT_PRINTF_TEST("%5*1d", 15, 42);
	FT_PRINTF_TEST("%0*1d", -15, 42);
	FT_PRINTF_TEST("%0*1d", 0, 42);
	FT_PRINTF_TEST("%0*1d", 15, 42);

	
	FT_PRINTF_TEST("%05.*s", -15, "az");
	FT_PRINTF_TEST("%05.*3s", 15, "az");
	FT_PRINTF_TEST("%05.*0s", 15, "az");
	FT_PRINTF_TEST("%05.*0s", -15, "az");
	FT_PRINTF_TEST("%05.*30s", 0, "az");
	FT_PRINTF_TEST("%05.*s", 0, "az");
	FT_PRINTF_TEST("%05.*3s", 0, "az");
	FT_PRINTF_TEST("%05.*3s", -15, "az");
	FT_PRINTF_TEST("%05.*1s", 15, "az");
	FT_PRINTF_TEST("%05.*1s", 0, "az");
	FT_PRINTF_TEST("%05.*1s", -15, "az");
	FT_PRINTF_TEST("%05*3s", -15, "az");
	FT_PRINTF_TEST("%05*3s", 0, "az");
	FT_PRINTF_TEST("%05*3s", 15, "az");
	FT_PRINTF_TEST("%5*3s", -15, "az");
	FT_PRINTF_TEST("%5*3s", 0, "az");
	FT_PRINTF_TEST("%5*3s", 15, "az");
	FT_PRINTF_TEST("%05*1s", -15, "az");
	FT_PRINTF_TEST("%05*1s", 0, "az");
	FT_PRINTF_TEST("%05*1s", 15, "az");
	FT_PRINTF_TEST("%5*1s", -15, "az");
	FT_PRINTF_TEST("%5*1s", 0, "az");
	FT_PRINTF_TEST("%5*1s", 15, "az");

	
	FT_PRINTF_TEST("%05.*S", 0, L"az");
	FT_PRINTF_TEST("%05.*S", -15, L"az");
	FT_PRINTF_TEST("%05.*S", 15, L"az");
	FT_PRINTF_TEST("%05.*0S", 15, L"az");
	FT_PRINTF_TEST("%05.*0S", -15, L"az");
	FT_PRINTF_TEST("%05.*0S", 0, L"az");
	FT_PRINTF_TEST("%05.*3S", 15, L"az");
	FT_PRINTF_TEST("%05.*3S", 0, L"az");
	FT_PRINTF_TEST("%05.*3S", -15, L"az");
	FT_PRINTF_TEST("%05.*1S", 15, L"az");
	FT_PRINTF_TEST("%05.*1S", 0, L"az");
	FT_PRINTF_TEST("%05.*1S", -15, L"az");
	FT_PRINTF_TEST("%05*3S", -15, L"az");
	FT_PRINTF_TEST("%05*3S", 0, L"az");
	FT_PRINTF_TEST("%05*3S", 15, L"az");
	FT_PRINTF_TEST("%5*3S", -15, L"az");
	FT_PRINTF_TEST("%5*3S", 0, L"az");
	FT_PRINTF_TEST("%5*3S", 15, L"az");
	FT_PRINTF_TEST("%05*1S", -15, L"az");
	FT_PRINTF_TEST("%05*1S", 0, L"az");
	FT_PRINTF_TEST("%05*1S", 15, L"az");
	FT_PRINTF_TEST("%5*1S", -15, L"az");
	FT_PRINTF_TEST("%5*1S", 0, L"az");
	FT_PRINTF_TEST("%5*1S", 15, L"az");


	FT_PRINTF_TEST("%05.*C", -15, L'a');
	FT_PRINTF_TEST("%05.*C", 0, L'a');
	FT_PRINTF_TEST("%05.*C", 15, L'a');
	FT_PRINTF_TEST("%05.*0C", -15, L'a');
	FT_PRINTF_TEST("%05.*0C", 0, L'a');
	FT_PRINTF_TEST("%05.*0C", 15, L'a');
	FT_PRINTF_TEST("%05.*3C", 15, L'a');
	FT_PRINTF_TEST("%05.*3C", 0, L'a');
	FT_PRINTF_TEST("%05.*3C", -15, L'a');
	FT_PRINTF_TEST("%05.*1C", 15, L'a');
	FT_PRINTF_TEST("%05.*1C", 0, L'a');
	FT_PRINTF_TEST("%05.*1C", -15, L'a');
	FT_PRINTF_TEST("%05*3C", -15, L'a');
	FT_PRINTF_TEST("%05*3C", 0, L'a');
	FT_PRINTF_TEST("%05*3C", 15, L'a');
	FT_PRINTF_TEST("%5*3C", -15, L'a');
	FT_PRINTF_TEST("%5*3C", 0, L'a');
	FT_PRINTF_TEST("%5*3C", 15, L'a');
	FT_PRINTF_TEST("%05*1C", -15, L'a');
	FT_PRINTF_TEST("%05*1C", 0, L'a');
	FT_PRINTF_TEST("%05*1C", 15, L'a');
	FT_PRINTF_TEST("%5*1C", -15, L'a');
	FT_PRINTF_TEST("%5*1C", 0, L'a');
	FT_PRINTF_TEST("%5*1C", 15, L'a');


	FT_PRINTF_TEST("%05.*3d %d", 15, 42, 44);
	FT_PRINTF_TEST("{%3*d}", 0, 0);
	FT_PRINTF_TEST("%3*d", 0, 1);
	FT_PRINTF_TEST("%3*d", -15, -10);
	FT_PRINTF_TEST("%3*d", 15, 10);
	FT_PRINTF_TEST("%*3d", 0, 1);
	FT_PRINTF_TEST("%*3d", -15, -10);
	FT_PRINTF_TEST("%*3d", 15, 10);
	FT_PRINTF_TEST("%*3*d", 0, 1, 2);
	FT_PRINTF_TEST("%*3*d", -15, -10, -20);
	FT_PRINTF_TEST("%*3*d", 15, 10, 20);
	FT_PRINTF_TEST("%*3*d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%*3*d %d", -15, -10, -20, -30);
	FT_PRINTF_TEST("%*3*d %d", 15, 10, 20, 30);

	FT_PRINTF_TEST("%.*d", 5, 10);
	FT_PRINTF_TEST("%.*d", -5, 10);
	FT_PRINTF_TEST("%.*s", -5, "az");
	FT_PRINTF_TEST("%*d|%*.10d|%10.*d", 5, 10, 6, 11, 7, 12);
	FT_PRINTF_TEST("%*.10d|%10.*d", 5, 10, 6, 11, 7, 12);
	FT_PRINTF_TEST("%*.10d|%10.*d", 15, 5, 11, 6);
	FT_PRINTF_TEST("%*.10d|%10.*d", 5, 12, 6, 11);
	FT_PRINTF_TEST("%*.*ls", 2, 3, L"我我");
	FT_PRINTF_TEST("%*.*lc", 2, 3, L'我');
	FT_PRINTF_TEST("%*.*ls", 5, 1, L"我我");
	FT_PRINTF_TEST("%*.*lc", 5, 1, L'我');
	FT_PRINTF_TEST("%*.*ls", 5, 1, NULL);
	FT_PRINTF_TEST("%*.*lc", 5, 1, 0);
	FT_PRINTF_TEST("%5**c", 0, 5, 48);
	#endif /* TETOI */

	#if TDOL == 1
	FT_PRINTF_TEST("----------------------------------------------\n");
	FT_PRINTF_TEST("%.*1$s", "aze", "rty");
	FT_PRINTF_TEST("%20.*d", -5, 4);
	FT_PRINTF_TEST("test . |%1$.*2$d|%1$d", 4, 10);
	FT_PRINTF_TEST("test . |%1$.*2$d", 4, 10);
	FT_PRINTF_TEST("|%1$.*2$d|%1$d", 4, 10);

	FT_PRINTF_TEST("%1$.*2$d", 5, 10);
	FT_PRINTF_TEST("%2$.*1$d", 5, 10);
	FT_PRINTF_TEST("%1$*2$d", 5, 10);
	FT_PRINTF_TEST("%2$*1$d", 5, 10);
	FT_PRINTF_TEST("test $ |%1$d", 4);
	FT_PRINTF_TEST("%1$c, %1$10c", '0');
	FT_PRINTF_TEST("%2$c, %1$10c", '0', 48);
	FT_PRINTF_TEST("%1$c, %1$d", '0');
	FT_PRINTF_TEST("|%1$.*2$s|%1$s", "qsd", 10);
	FT_PRINTF_TEST("%1$*2$s|%1$s", "qsd", 10);
	# if SETLOCAL_ENABLE != 0
	setlocale(LC_ALL, "fr_FR.UTF-8");
	# endif
	wchar_t wstr = L'\u732b';
	char *str;
	str = (char *)&wstr;
	FT_PRINTF_TEST(" : %1$s %2$d S:%2$lc %3$x %4$x %3$u %4$u", str, wstr, *str, *(str + 1));
	# if SETLOCAL_ENABLE != 0
	setlocale(LC_ALL, "");
	# endif
	FT_PRINTF_TEST("%2$d|%10$d|%3$d|%4$d|%5$d|%6$d|%7$d|%8$d|%9$d|%1$d", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
	FT_PRINTF_TEST("%3$*2$d|%10$d|%4$d|%5$d|%6$d|%7$d|%9$*8$d|%1$d", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
	FT_PRINTF_TEST("%2$*3$d|%10$d|%4$d|%5$d|%6$d|%7$d|%9$*8$d|%1$d", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", INT_MAX);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", INT_MIN);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", -1);
	FT_PRINTF_TEST("%1$c %1$u %1$d %1$hhd %1$hd %1$ld %1$lld", 255);
	FT_PRINTF_TEST("%1$c %1$u %1$d %1$hhd %1$hd %1$ld %1$lld", 256);
	FT_PRINTF_TEST("%1$c %1$u %1$x %1$hhu %1$hu %1$lu %1$llu", 256);
	FT_PRINTF_TEST("%1$c %1$x %1$hho %1$hu %1$c", -1);
	FT_PRINTF_TEST("%1$c %1$u %1$x %1$hho %1$hu", -1);
	FT_PRINTF_TEST("%1$c %1$u %1$x %1$hho %1$hu %1$c", 255);
	FT_PRINTF_TEST("%1$c %1$u %1$x %1$hho %1$hu", 255);
	FT_PRINTF_TEST("%1$c %1$u", -1);
	
	FT_PRINTF_TEST("%*1$3*2$3$d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%*1$3*2$3$d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%*1$3*2$3$d %d", -15, -10, -20, -30);
	FT_PRINTF_TEST("%*1$3*2$3$d %d", 15, 10, 20, 30);
	FT_PRINTF_TEST("%*1$3*2$3$d %4$d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%*1$3*2$3$d %4$d", -15, -10, -20, -30);
	FT_PRINTF_TEST("%*1$3*2$3$d %4$d", 15, 10, 20, 30);
	FT_PRINTF_TEST("%2$05.*1$3d", 15, 42, 50);
	FT_PRINTF_TEST("%2$05.*1$3d %3$d", 15, 42, 44);
	FT_PRINTF_TEST("%$d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%2$$d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%$2$5d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%2$d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%3$d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%4$d", 0, 1, 2, 3);

	FT_PRINTF_TEST("%*$d", 3, 2, 1);
	FT_PRINTF_TEST("%1$d", 4, 10);
	FT_PRINTF_TEST("%1$.*2$d", 4, 10);
	FT_PRINTF_TEST("%$$$d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%2$$1$$d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%$d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%2$$d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%$$$d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%2$$1$$d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%d %$d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%d %2$$d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%d %$$$d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%d %2$$1$$d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%1$ls %1$s", L"aze");
	FT_PRINTF_TEST("%1$lc %1$c", 49);
	FT_PRINTF_TEST("%1$s %1$ls", L"aze");
	FT_PRINTF_TEST("%1$c %1$lc", L'3');
	FT_PRINTF_TEST("%1$s %1$ls", L"我我");
	FT_PRINTF_TEST("%1$c %1$lc", L'我');
	FT_PRINTF_TEST("%1$ls %1$s", L"我我");
	FT_PRINTF_TEST("%1$lc %1$c", L'我');
	FT_PRINTF_TEST("%d %d %*1$.*1$ls", 5, 1, NULL);
	FT_PRINTF_TEST("%d %d %*1$.*1$lc", 5, 1, 0);
	FT_PRINTF_TEST("%3$*3$.*3$ls", 5, 1, NULL);
	FT_PRINTF_TEST("%3$*3$.*3$lc", 5, 1, 0);
	//FT_PRINTF_TEST("%3$*3$.*3$ls", 5, 1, L"我我");// full printf
	//FT_PRINTF_TEST("%3$*3$.*3$lc", 5, 1, L'我');// full printf
	FT_PRINTF_TEST("%.*1$d", LONG_MAX);
	FT_PRINTF_TEST("%.*1$hd", LONG_MAX);
	FT_PRINTF_TEST("%.*1$hhd", LONG_MAX);
	FT_PRINTF_TEST("%.*1$ld", LONG_MAX);
	FT_PRINTF_TEST("%.*1$lld", LONG_MAX);
	FT_PRINTF_TEST("%.*1$s", "aze", 0);
	FT_PRINTF_TEST("%*1$hd", LONG_MAX);
	FT_PRINTF_TEST("%*1$hhd", LONG_MAX);
	FT_PRINTF_TEST("%*1$ld", LONG_MAX);
	FT_PRINTF_TEST("%*1$lld", LONG_MAX);
	FT_PRINTF_TEST("%*1$c", 49, 0);// large write
	FT_PRINTF_TEST("%*1$c", 2, 0);
	FT_PRINTF_TEST("%*1$s", 0, 0);
	FT_PRINTF_TEST("%.*1$d", 5, 2);
	FT_PRINTF_TEST("%.*1$hd", 5, 2);
	FT_PRINTF_TEST("%.*1$hhd", 5, 2);
	FT_PRINTF_TEST("%.*1$ld", 5, 2);
	FT_PRINTF_TEST("%.*1$lld", 5, 2);
	FT_PRINTF_TEST("%.*1$s", "aze", "rty");
	FT_PRINTF_TEST("%.*1$c", 2, 0);
	FT_PRINTF_TEST("%*1$d", 5, 2);
	FT_PRINTF_TEST("%*1$hd", 5, 2);
	FT_PRINTF_TEST("%*1$hhd", 5, 2);
	FT_PRINTF_TEST("%*1$ld", 5, 2);
	FT_PRINTF_TEST("%*1$lld", 5, 2);
	FT_PRINTF_TEST("%*1$s", 0, "");
	FT_PRINTF_TEST("%.*1$d %.*2$d", 5, 2, 10, 8);
	FT_PRINTF_TEST("%.*1$hd %.*2$hd", 5, 2, 10, 8);
	FT_PRINTF_TEST("%.*1$hhd %.*2$hhd", 5, 2, 10, 8);
	FT_PRINTF_TEST("%.*1$ld %.*2$ld", 5, 2, 10, 8);
	FT_PRINTF_TEST("%.*1$lld %.*2$lld", 5, 2, 10, 8);
	FT_PRINTF_TEST("%.*1$s %.*2$s", "aze", "rty", "qsd", "fgh");
	FT_PRINTF_TEST("%.*1$c %.*2$c", 2, 0, 4, 6);

	FT_PRINTF_TEST("%d %.*1$d", 5, 2, 9);
	FT_PRINTF_TEST("%d %.*1$hd", 5, 2, 9);
	FT_PRINTF_TEST("%d %.*1$hhd", 5, 2, 9);
	FT_PRINTF_TEST("%d %.*1$ld", 5, 2, 9);
	FT_PRINTF_TEST("%d %.*1$lld", 5, 2, 9);
	FT_PRINTF_TEST("%d %.*1$s", 9, "aze", "rty");
	FT_PRINTF_TEST("%d %.*1$c", 2, 0, 9);
	FT_PRINTF_TEST("%d %*1$d", 5, 2, 9);
	FT_PRINTF_TEST("%d %*1$hd", 5, 2, 9);
	FT_PRINTF_TEST("%d %*1$hhd", 5, 2, 9);
	FT_PRINTF_TEST("%d %*1$ld", 5, 2, 9);
	FT_PRINTF_TEST("%d %*1$lld", 5, 2, 9);
	FT_PRINTF_TEST("%d %.*1$d %.*2$d", 5, 2, 10, 8, 9);
	FT_PRINTF_TEST("%d %.*1$hd %.*2$hd", 5, 2, 10, 8, 9);
	FT_PRINTF_TEST("%d %.*1$hhd %.*2$hhd", 5, 2, 10, 8, 9);
	FT_PRINTF_TEST("%d %.*1$ld %.*2$ld", 5, 2, 10, 8, 9);
	FT_PRINTF_TEST("%d %.*1$lld %.*2$lld", 5, 2, 10, 8, 9);
	FT_PRINTF_TEST("%d %*1$s", 0, "", 9);
	FT_PRINTF_TEST("%d %.*1$s %.*1$s", 8, "aze", "rty", "qsd", "fgh");
	FT_PRINTF_TEST("%d %.*1$c %.*2$c", 2, 0, 4, 6, 8);

	FT_PRINTF_TEST("%.*1$d %.*1$d", 5, 2, 10, 8);
	FT_PRINTF_TEST("%.*1$hd %.*1$hd", 5, 2, 10, 8);
	FT_PRINTF_TEST("%.*1$hhd %.*1$hhd", 5, 2, 10, 8);
	FT_PRINTF_TEST("%.*1$ld %.*1$ld", 5, 2, 10, 8);
	FT_PRINTF_TEST("%.*1$lld %.*1$lld", 5, 2, 10, 8);

	FT_PRINTF_TEST("%.*5$s %.*5$s", "aze", "rty", "qwe", "fgh", 5, "wxc");
	FT_PRINTF_TEST("%.*2$s", "aze", 2, "qwe");
	FT_PRINTF_TEST("%.*3$s %.*3$s", "aze", "rty", 3, "qsd");

	FT_PRINTF_TEST("%5$.*d %5$.*d", 1, 2, 3, 4, 5, 6, 7, 8);
	FT_PRINTF_TEST("%2$.*d %2$.*d", 1, 2, 3, 4, 5, 6, 7, 8);
	FT_PRINTF_TEST("%3$.*d %3$.*d", 1, 2, 3, 4, 5, 6, 7, 8);

	FT_PRINTF_TEST("%.*4$s %.*3$s", "aze", "rty", 3, 5);
	FT_PRINTF_TEST("%5$.*d %4$.*d", 1, 2, 3, 4, 5, 6, 7, 8);
	FT_PRINTF_TEST("%2$.*d %1$.*d", 1, 2, 3, 4, 5, 6, 7, 8);
	FT_PRINTF_TEST("%3$.*d %2$.*d", 1, 2, 3, 4, 5, 6, 7, 8);
	FT_PRINTF_TEST("%2#$hd", 5, 6);
	FT_PRINTF_TEST("%-5#$hd", 5, 6);
	FT_PRINTF_TEST("%2$d %d", 0, 1, 2, 3);
	
	#endif /* TDOL */

	#if TEXTRA == 1
	FT_PRINTF_TEST("%b %lb %hb", 123, 456, 789);
	FT_PRINTF_TEST("%b %lb %hb", LONG_MAX, LONG_MAX, LONG_MAX);
	FT_PRINTF_TEST("%B %lB %hB", LONG_MAX, LONG_MAX, LONG_MAX);
	FT_PRINTF_TEST("%#b %#lb %#hb", 123, 456, 789);
	FT_PRINTF_TEST("%#b %#lb %#hb", LONG_MAX, LONG_MAX, LONG_MAX);
	FT_PRINTF_TEST("%#B %#lB %#hB", LONG_MAX, LONG_MAX, LONG_MAX);
	FT_PRINTF_TEST("%05.7d", 2);
	FT_PRINTF_TEST("%...d %d", 0, 1, 2, 3);
	FT_PRINTF_TEST("%   %", 0);
	FT_PRINTF_TEST("%5%", 456);
	FT_PRINTF_TEST("%-5%", 456);
	FT_PRINTF_TEST("%.0%", 456);
	FT_PRINTF_TEST("%lhh", "2147483647");
	FT_PRINTF_TEST("%d %.0%", 1);
	FT_PRINTF_TEST("%.0% %d", 1);
	FT_PRINTF_TEST("%.0%");
	FT_PRINTF_TEST("%20+10d%", 5, 10);

	FT_PRINTF_TEST("%   %", 0);	
	FT_PRINTF_TEST("%d %10R", 1);
	FT_PRINTF_TEST("{%10lR%}d", 1);
	FT_PRINTF_TEST("{%10lR%d", 1);
	FT_PRINTF_TEST("%d %10lR", 1);
	FT_PRINTF_TEST("{%10R}", 456);
	FT_PRINTF_TEST("{%10R%}", 456);
	FT_PRINTF_TEST("{%R}", 456);
	FT_PRINTF_TEST("%.0% %%", 456);
	FT_PRINTF_TEST("%%", 456);
	FT_PRINTF_TEST("%.0% %d", 1);
	FT_PRINTF_TEST("%d %.0%", 1);
	FT_PRINTF_TEST("%20+10d|%", 5, 10);
	FT_PRINTF_TEST("% ", 456);
	FT_PRINTF_TEST("%", 456);
	FT_PRINTF_TEST("% ", 456);
	FT_PRINTF_TEST("%", 456);
	FT_PRINTF_TEST("% hh", 456);
	FT_PRINTF_TEST("% h", 456);
	FT_PRINTF_TEST("% ll", 456);
	FT_PRINTF_TEST("% l", 456);
	FT_PRINTF_TEST("% L", 456);
	FT_PRINTF_TEST("% q", 456);
	FT_PRINTF_TEST("% j", 456);
	FT_PRINTF_TEST("% z", 456);
	FT_PRINTF_TEST("% t", 456);
	FT_PRINTF_TEST("% tt", 456);
	FT_PRINTF_TEST("% lll", 456);

	
	FT_PRINTF_TEST("%d aze %% %d%d %c a %lc %s ", 1, 2, 3, 'A', (wchar_t)'A', "qsdf");
	FT_PRINTF_TEST("% llllqhhhLjztllllqhhhLjzt", 456);
	FT_PRINTF_TEST("% llllqhhhLjztllllqhhhLjztd", UINT_MAX);
	FT_PRINTF_TEST("% llllqhhhLjztllllqhhhLjztd", -1);
	FT_PRINTF_TEST("% llllqhhhLjztllllqhhhLjztw", 456);
	FT_PRINTF_TEST("% llllqhhhLjztllllqhhhLjzt#x", 456);
	FT_PRINTF_TEST("% llllqhhhLjzTtllllqhhhLjzt#x", 456);
	FT_PRINTF_TEST("% hhhllllllLqjzttzjqdLllllllhhh", UINT_MAX);

	
	FT_PRINTF_TEST("%-10.5d", 13);
	FT_PRINTF_TEST("%-10.5s", "13");
	FT_PRINTF_TEST("%0-10.5d", 13);
	FT_PRINTF_TEST("%0-10.5s", "13");
	FT_PRINTF_TEST("%0-10.d", 13);
	FT_PRINTF_TEST("%0-10.s", "13");
	FT_PRINTF_TEST("%0-10d", 13);
	FT_PRINTF_TEST("%0-10s", "13");
	FT_PRINTF_TEST("%+0-10.5d", 13);
	FT_PRINTF_TEST("%+0-10.5s", "13");
	FT_PRINTF_TEST("%+0-10.d", 13);
	FT_PRINTF_TEST("%+0-10.s", "13");
	FT_PRINTF_TEST("%+0-10d", 13);
	FT_PRINTF_TEST("%+0-10s", "13");
	FT_PRINTF_TEST("%0+10.5d", 13);
	FT_PRINTF_TEST("%0+10.5s", "13");
	FT_PRINTF_TEST("%0+10.5c", 49);
	FT_PRINTF_TEST("%0+10.d", 13);
	FT_PRINTF_TEST("%0+10.s", "13");
	FT_PRINTF_TEST("%0+10.c", 49);
	FT_PRINTF_TEST("%0+10d", 13);
	FT_PRINTF_TEST("%0+10s", "13");
	FT_PRINTF_TEST("%0+10c", 49);

	#ifndef __linux__
	FT_PRINTF_TEST("%####0000 33..1..#00dw", 256);
	FT_PRINTF_TEST("%####0000 33..1..#dw", 256);
	FT_PRINTF_TEST("%####0000 33..1..d", 256);
	FT_PRINTF_TEST("%####000033..1..d", 256);
	FT_PRINTF_TEST("%-+ ####000033..1..d", 256);
	FT_PRINTF_TEST("%-+ ####..1..d", 256);
	FT_PRINTF_TEST("%####0000 33..1..#00d", 256);
	FT_PRINTF_TEST("%####0000 33..5..#00d", 256);
	FT_PRINTF_TEST("%####0000 33.5...#00d", 256);
	FT_PRINTF_TEST("%####0000 33.5...#010d", 256);
	FT_PRINTF_TEST("%####0000 33.5...4#10d", 256);
	FT_PRINTF_TEST("%50-+ ####..1..d", 256);
	FT_PRINTF_TEST("%-+ ####..1..d", 256);
	FT_PRINTF_TEST("% -+####..1..d", 256);
	#endif
	FT_PRINTF_TEST("%-+ #.1d", 256);
	FT_PRINTF_TEST("%#d %#u", UINT_MAX, UINT_MAX);
	FT_PRINTF_TEST("%-+ .1d", 256);
	FT_PRINTF_TEST("%+2d %+3d %+d %+.d", 256, 256, 256, 256);
	FT_PRINTF_TEST("%+3.3d %+5.5d %+6.5d %+5.6d", 256, 256, 256, 256);
	FT_PRINTF_TEST("% -+#20.1d", 256);
	FT_PRINTF_TEST("%50-+ 0#.1d", 256);
	FT_PRINTF_TEST("%50-+ #.1d", 256);
	FT_PRINTF_TEST("%5%", 13);
	FT_PRINTF_TEST("%-5%", 13);
	FT_PRINTF_TEST("%5%l", 13);
	FT_PRINTF_TEST("%-5%l", 13);
	FT_PRINTF_TEST("%5%d", 13);
	FT_PRINTF_TEST("%-5%d", 13);
	FT_PRINTF_TEST("%-5%x", 13);
	FT_PRINTF_TEST("%-5#%x", 13);

	FT_PRINTF_TEST("%.3%");
	FT_PRINTF_TEST("%#.3%");
	FT_PRINTF_TEST("%0.3%");
	FT_PRINTF_TEST("%10#0.3%");
	FT_PRINTF_TEST("%+%");
	FT_PRINTF_TEST("% 0%");

	/* FT_PRINTF_TEST("%1$ld %1$d %1$hhd %1$hd %1$ld %1$lld %1$c", LONG_MAX, 1, 2); */ // promotion
	/* FT_PRINTF_TEST("%1$ld %1$d %1$hhd %1$hd %1$ld %1$lld %1$c", 1, 2, 3, 4, 5); */ // promotion
	/* FT_PRINTF_TEST("%1$lld %1$ld %1$hd %1$hhd %1$d %1$c", LONG_MIN); */ //promotion
	//FT_PRINTF_TEST("%1$d %1$ld  %1$hhd %1$hd %1$ld %1$lld %1$c", LONG_MAX, 1, 2);// promotion
	//FT_PRINTF_TEST("%1$d %1$ld %1$hhd %1$hd %1$ld %1$lld %1$c", 1, 2, 3, 4, 5);// promotion
	FT_PRINTF_TEST("%1$hd %1$ld %1$hhd %1$d %1$c %1$lld", LONG_MIN);
	//FT_PRINTF_TEST("%1$llu %1$u", LONG_MAX);
	FT_PRINTF_TEST("%1$u %1$llu", LONG_MAX);
	//FT_PRINTF_TEST("%1$lld %1$d", LONG_MIN);
	//FT_PRINTF_TEST("%1$lld %1$d", LONG_MAX);

	FT_PRINTF_TEST("%1$d %1$lld", LONG_MIN);
	FT_PRINTF_TEST("%1$hhd %1$lld", LONG_MIN);
	FT_PRINTF_TEST("%1$d %1$hhd %1$lld", LONG_MIN);
	FT_PRINTF_TEST("%1$c %1$lld", LONG_MIN);
	FT_PRINTF_TEST("%1$c %1$lld", (long long)68);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", LLONG_MAX);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", LONG_MAX);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", INT_MAX);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", SHRT_MAX);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", CHAR_MAX);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", LLONG_MIN);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", LONG_MIN);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", INT_MIN);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", SHRT_MIN);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", CHAR_MIN);
	FT_PRINTF_TEST("%1$u %1$hhu %1$hu %1$lu %1$llu", ULLONG_MAX);
	FT_PRINTF_TEST("%1$u %1$hhu %1$hu %1$lu %1$llu", ULONG_MAX);
	FT_PRINTF_TEST("%1$u %1$hhu %1$hu %1$lu %1$llu", UINT_MAX);
	FT_PRINTF_TEST("%1$u %1$hhu %1$hu %1$lu %1$llu", USHRT_MAX);
	FT_PRINTF_TEST("%1$u %1$hhu %1$hu %1$lu %1$llu", UCHAR_MAX);

	FT_PRINTF_TEST("%1$hhd %1$c %1$hd %1$lld %1$d %1$ld", LONG_MIN);
	FT_PRINTF_TEST("%1$c %1$d %1$hhd %1$hd %1$ld %1$lld", LONG_MAX);


	FT_PRINTF_TEST("%d %d %d %d %d %d", -1, 0, INT_MAX, INT_MIN, 123, -456);
	FT_PRINTF_TEST("%1$d %1$lld", LONG_MAX);
	FT_PRINTF_TEST("%1$ld %1$lld", LONG_MAX);
	FT_PRINTF_TEST("%1$lld %1$ld", LONG_MAX);
	FT_PRINTF_TEST("%1$lld %1$lld", LONG_MAX);

	FT_PRINTF_TEST("%1$ld %1$lld", LONG_MIN);
	FT_PRINTF_TEST("%1$lld %1$ld", LONG_MIN);
	FT_PRINTF_TEST("%1$lld %1$lld", LONG_MIN);

	FT_PRINTF_TEST("%1$u %1$llu", LONG_MAX);
	FT_PRINTF_TEST("%1$lu %1$llu", LONG_MAX);
	FT_PRINTF_TEST("%1$llu %1$lu", LONG_MAX);
	FT_PRINTF_TEST("%1$llu %1$llu", LONG_MAX);
	FT_PRINTF_TEST("%1$lc %1$lc", L'a');
	/* printf("ft_printf NULL ");fflush(stdout); */
	/* ft_printf(NULL, NULL); */
	/* printf("\n");fflush(stdout); */
	/* printf("   printf NULL ");fflush(stdout); */
	/* printf(NULL, NULL);fflush(stdout); */
	/* printf("\n");fflush(stdout); */
	FT_PRINTF_TEST("");
	FT_PRINTF_TEST("\0");
	FT_PRINTF_TEST("%.5d", 5);// compare
	FT_PRINTF_TEST("%hhd", 5, 2);
	#endif /* TEXTRA */


	//FT_PRINTF_TEST("%*1$s", "aze", "rty");// full printf
	//FT_PRINTF_TEST("%05.*3f", 15., 42.);
	//FT_PRINTF_TEST("%05.*3a", 15., 42.);
	//FT_PRINTF_TEST(". |%1$.*2$f", (double)10.123, 22., 33., 44.);
	//FT_PRINTF_TEST(". |%1$.*2$f", (double)10.123, 10., 15., 15.);

	#if TATT_N == 1
	int	att_n[1];
	FT_PRINTF_TEST("%n", att_n);
	FT_PRINTF_TEST("retour precedent %d", *att_n);
	*att_n = 0;
	FT_PRINTF_TEST("testtesttest -%n+ ", att_n);
	FT_PRINTF_TEST("retour precedent %d", *att_n);
	*att_n = 0;
	FT_PRINTF_TEST("testtesttest -%hn+ ", att_n);
	FT_PRINTF_TEST("retour precedent %hd", *att_n);
	*att_n = 0;
	FT_PRINTF_TEST("testtesttest -%hhn+ ", att_n);
	FT_PRINTF_TEST("retour precedent %hhd", *att_n);
	*att_n = 0;
	FT_PRINTF_TEST("testtesttest -%ln+ ", att_n);
	FT_PRINTF_TEST("retour precedent %ld", *att_n);
	*att_n = 0;
	FT_PRINTF_TEST("testtesttest -%lln+ ", att_n);
	FT_PRINTF_TEST("retour precedent %lld", *att_n);
	*att_n = 0;
	FT_PRINTF_TEST("testtesttest -%jn+ ", att_n);
	FT_PRINTF_TEST("retour precedent %jd", *att_n);
	*att_n = 0;
	FT_PRINTF_TEST("testtesttest -%zn+ ", att_n);
	FT_PRINTF_TEST("retour precedent %zd", *att_n);
	*att_n = 0;
	FT_PRINTF_TEST("testtesttest -%tn+ ", att_n);
	FT_PRINTF_TEST("retour precedent %td", *att_n);
	#endif /* TATT_N */

	# if SETLOCAL_ENABLE != 0
	finish = clock();
	printf("\n\nTime: %ld ticks\n", finish - start);
	# endif
}

void test(void)
{
  int j,i;

  //FT_PRINTF_TEST("%f %1$d", 5., 4);
  //FT_PRINTF_TEST("%1$f %1$d", 5., 4);
  
  /* FT_PRINTF_TEST("%lld %llu", DBL_MAX, DBL_MAX);// alea sur arg 2 */
  /* FT_PRINTF_TEST("%lLd %lLu", (long double)LONG_MAX + 1, (long double)ULONG_MAX + 1);// arg 2 alea */
  /* FT_PRINTF_TEST("%lLd %lLu", DBL_MAX, DBL_MAX);// arg 2 alea */
  /* FT_PRINTF_TEST("%Ld %Lu", DBL_MAX, DBL_MAX);// alea sur arg 2 */
  /* FT_PRINTF_TEST("%ld %lu", (long double)LONG_MAX + 1, (long double)ULONG_MAX + 1); */
  /* FT_PRINTF_TEST("%Ld %Lu", (long double)LONG_MAX + 1, (long double)ULONG_MAX + 1); */

}


int	main(void)
{
	maintest();
	return (0);
}
