/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 09:03:04 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/26 07:31:51 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlen(char const *s)
{
	int len;

	if (!s)
		return (0);
	len = 0;
	while (s[len])
		++len;
	return (len);
}

/* size_t		ft_strlen(char const *s) */
/* { */
/* 	unsigned int	*is; */

/* 	if (!s) */
/* 		return (0); */
/* 	is = (unsigned int *)s; */
/* 	while (1) */
/* 	{ */
/* 		if (!(*is & (unsigned int)0xFF)) */
/* 			return ((char *)is - (char *)s); */
/* 		if (!(*is & (unsigned int)0xFF00)) */
/* 			return ((char *)is - (char *)s + 1); */
/* 		if (!(*is & (unsigned int)0xFF0000)) */
/* 			return ((char *)is - (char *)s + 2); */
/* 		if (!(*is & (unsigned int)0xFF000000)) */
/* 			return ((char *)is - (char *)s + 3); */
/* 		++is; */
/* 	} */
/* } */
