#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/18 06:53:03 by mmichel           #+#    #+#              #
#    Updated: 2016/05/30 02:52:48 by mmichel          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

LIBPRINTF := ../ft_printf
TFLOAT = 0
TDIBOUX = 0
TSC = 0
TETOI = 0
TDOL = 0
TEXTRA = 0
TATT_N = 0

PREFIX	:= .
INC := $(PREFIX)/inc
LIBS := $(PREFIX)
SRC := $(PREFIX)/src

VPATH = $(SRC):$(SRC)/lib:$(INC):$(LIBS)

SRCLIBC = $(shell find $(SRC) -type f | grep ".c$$" \
	| grep -vE "(/\.|/\#)" )
#	| grep -vE "(/\.|main\.c|/\#)" )
OBJ = $(SRCLIBC:.c=.o)

NAME := diff
CC := gcc
LDLIBS := -L$(LIBPRINTF)
LDFLAGS := -lftprintf
CFLAGS := -I$(INC) -g
OBJGCOV = $(shell find $(LIBPRINTF) -type f | grep ".c$$" \
	| grep -vE "(/\.|/\#)" )

BINTEST = test.bin
EXTRACLEAN = $(shell find $(LIBPRINTF) $(SRC) -type f | grep -E "(.gcov|.gcno|.gcda)$$")
EXTRACLEAN += $(shell find . $(LIBPRINTF) -maxdepth 1 -type f \
	| grep -E "(.gcov|.gcno|.gcda)$$")

UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
	ECHO := echo
#	OPT_TEST = | awk -F'~' '{i=length($$2) ; if ( i > 80 ) { s1 = substr($$2, 1, 25) ; s2 = substr($$2, i - 25, 25) ; $$2 = s1"[...]"s2 } ; print }'
else
	ECHO := echo -e
#	OPT_TEST = | awk -F'~' '{i=length($$2) ; if ( i > 80 ) { s1 = substr($$2, 1, 25) ; s2 = substr($$2, i - 25, 25) ; $$2 = s1"[...]"s2 } ; print }'
endif

ifndef DIFF_H
DIFF_H = 200
endif
ifdef ALL
TFLOAT = $(ALL)
TDIBOUX = $(ALL)
TSC = $(ALL)
TETOI = $(ALL)
TDOL = $(ALL)
TEXTRA = $(ALL)
TATT_N = $(ALL)
endif
ifdef FMT
EXTRACFLAGS += -DFT_PRINTF_TEST=FT_PRINTF_TEST_FMT
endif
ifdef DLINE
EXTRACFLAGS += -DDLINE=$(DLINE)
endif
ifdef DLINE_IN_LINE
EXTRACFLAGS += -DDLINE_IN_LINE=$(DLINE_IN_LINE)
endif
ifdef SETLOCAL_ENABLE
EXTRACFLAGS += -DSETLOCAL_ENABLE=$(SETLOCAL_ENABLE)
endif

all: re $(BINTEST) diff

clean:
	$(MAKE) $(GNUMAKEFLAGS) -C $(LIBPRINTF) clean
	$(RM) -- $(OBJ)

fclean:
	$(MAKE) $(GNUMAKEFLAGS) -C $(LIBPRINTF) fclean
ifneq (,$(findstring .g, $(EXTRACLEAN)))
	$(RM) -- $(EXTRACLEAN)
endif
	$(RM) -- $(OBJ)
	$(RM) -- $(BINTEST) test.db result.txt gmon.out
	$(RM) -r -- html $(LIBPRINTF)/gcov.log
	$(RM) -r -- diff_ft diff_pr

re: fclean test

-lftprintf:
	@$(ECHO) Compile $@
ifeq ($(UNAME),Darwin)
	$(MAKE) $(GNUMAKEFLAGS) -C $(LIBPRINTF) -j 4 CFLAGS="-Wall -Werror -Wextra $(CFLAGS)"
else
	$(MAKE) $(GNUMAKEFLAGS) -C $(LIBPRINTF) -j 8 CFLAGS="-Wall -Werror -Wextra $(CFLAGS)"
endif

%.o: %.c
	$(CC) -c $(CFLAGS) $(EXTRACFLAGS) -Wno-format $< -o $@

$(BINTEST): EXTRACFLAGS += -DTFLOAT=$(TFLOAT)
$(BINTEST): EXTRACFLAGS += -DTDIBOUX=$(TDIBOUX)
$(BINTEST): EXTRACFLAGS += -DTSC=$(TSC)
$(BINTEST): EXTRACFLAGS += -DTETOI=$(TETOI)
$(BINTEST): EXTRACFLAGS += -DTDOL=$(TDOL)
$(BINTEST): EXTRACFLAGS += -DTEXTRA=$(TEXTRA)
$(BINTEST): EXTRACFLAGS += -DTATT_N=$(TATT_N)
$(BINTEST): -lftprintf $(OBJ)
	@$(ECHO) "Compile main"
	$(CC) $(CFLAGS) $(EXTRACFLAGS) -Wno-format -o $@ $^ $(LDLIBS)

diff: $(BINTEST)
	@$(ECHO) "Gen diff"
	@grep -a -- ft_ result.txt | sed -e 's/ft_/   /' > diff_ft
	@grep -av -- ft_ result.txt | grep -a -- printf > diff_pr
	@sed 's/    printf://' diff_ft > diff_ft2 && mv diff_ft2 diff_ft
	@sed 's/    printf://' diff_pr > diff_pr2 && mv diff_pr2 diff_pr
	-@diff -a -y --suppress-common-lines -W $(DIFF_H) diff_ft diff_pr
	@$(ECHO) "Nombre de difference: "
	-@diff -a -y --suppress-common-lines -W $(DIFF_H) diff_ft diff_pr | wc -l
	@$(RM) diff_ft diff_pr

diff_comp: -lftprintf $(OBJ)
	@grep -a -- ft_ result.txt | sed -e 's/ft_/   /' > diff_ft
	@grep -av -- ft_ result.txt | grep -a -- printf > diff_pr
	@sed 's/    printf://' diff_ft > diff_ft2 && mv diff_ft2 diff_ft
	@sed 's/    printf://' diff_pr > diff_pr2 && mv diff_pr2 diff_pr
	-@diff -a -y --suppress-common-lines -W $(DIFF_H) diff_ft diff_pr > res_diff.txt
	$(ECHO) '#include "ft_printf_test.h"\nvoid     testmain(void)\n{\n        int i, j;' > main_diff.c
	for i in $$(awk -v wcl=$$(($$(wc -l res_diff.txt | awk '{print $$1}') - 2)) '{if (NR < wcl) print $$1}' res_diff.txt);\
	do sed -n "$$i p" $(SRC)/main.c >> main_diff.c; done
	$(ECHO) '}\nint     main(void)\n{\n        testmain();\n}' >>main_diff.c
	$(CC) $(CFLAGS) $(EXTRACFLAGS) -Wno-format -o main_diff.o main_diff.c $^ $(LDLIBS)
	./main_diff.o $(OPT_TEST)
	@$(RM) main_diff.c res_diff.txt main_diff.o

db: CFLAGS += -DDEBUG
db: OPT_TEST = ""
db: fclean $(BINTEST)
	@$(ECHO) "---------------------------------------\n"
	./$(BINTEST)
	@$(ECHO) "\n---------------------------------------"


gcov: CFLAGS += -fprofile-arcs -ftest-coverage
gcov: re test $(OBJ)
	@$(ECHO) Genere gcov.log
	cd $(LIBPRINTF) && gcov $(OBJGCOV) -f > gcov.log
	-@mkdir html
	@$(ECHO) chmod -R o=rwX $(LIBPRINTF)
	lcov -c -d $(LIBPRINTF) -o html/lcov.txt
	@$(ECHO) chmod -R o=rX $(LIBPRINTF)
	cd $(LIBPRINTF) && gcov $(OBJGCOV) -f > gcov.log
	@$(ECHO) Genere index.html in html
	genhtml html/lcov.txt -o html
	@$(ECHO) uri : file://`pwd`/html/index.html

gcovdb: CFLAGS += -DDEBUG
gcovdb: gcov

gprof: CFLAGS += -pg
gprof: re test
	gprof --display-unused-functions -b $(BINTEST) $(OPT_TEST)

mem: EXTRACFLAGS += -DFT_PRINTF_TEST=FT_PRINTF_ONLY_FT
mem: EXTRACFLAGS += -DSETLOCAL_ENABLE=0
mem: $(BINTEST)
	@$(ECHO) Execute valgrind
	valgrind --tool=memcheck --leak-check=full --show-reachable=yes \
	--num-callers=20 --track-fds=yes --track-origins=yes \
	--read-var-info=yes --leak-resolution=high --track-origins=yes \
	./$^ $(OPT_TEST)

memdb: EXTRACFLAGS += -DFT_PRINTF_TEST=FT_PRINTF_ONLY_FT
memdb: EXTRACFLAGS += -DSETLOCAL_ENABLE=0
memdb: CFLAGS += -DDEBUG
memdb: $(BINTEST)
	@$(ECHO) Execute valgrind
	valgrind --tool=memcheck --leak-check=yes --show-reachable=yes \
	--num-callers=20 --track-fds=yes --track-origins=yes \
	--read-var-info=yes --leak-resolution=high --track-origins=yes -v \
	./$^ $(OPT_TEST)

result.txt:
test: $(BINTEST)
	@$(ECHO) "---------------------------------------\n"
	@$(ECHO) "Execute $^ && Result in result.txt"
	./$^ $(OPT_TEST) 2>&1 > result.txt
	@$(ECHO) "\n---------------------------------------"

align: CFLAGS += -Wpadded
align: $(BINTEST)

help:
	@$(ECHO) "Usage : make [OPTION]..."
	@$(ECHO) "\nOPTION :"
	@$(ECHO) "\tall             re, execute et diff"
	@$(ECHO) "\tdiff            compile, execute et compare les résulta obtenu"
	@$(ECHO) "\tdiff_comp       compile seulement avec les lignes du 'diff' en erreur"
	@$(ECHO) "\ttest            compile et execute"
	@$(ECHO) "\tdb              clean et test en mode debug"
	@$(ECHO) "\tgcov            recompile, execute et lance gcov"
	@$(ECHO) "\tgp              recompile, execute et lance gprof"
	@$(ECHO) "\tmem             compile et lance valgrind"
	@$(ECHO) "\tTFLOAT=         0 = desactive, 1 = active, defaut 0"
	@$(ECHO) "\tTDIBOUX=            0 = desactive, 1 = active, defaut 0"
	@$(ECHO) "\tTETOI=         0 = desactive, 1 = active, defaut 0"
	@$(ECHO) "\tTDOL=           0 = desactive, 1 = active, defaut 0"
	@$(ECHO) "\tTSC=            0 = desactive, 1 = active, defaut 0"
	@$(ECHO) "\tTEXTRA=         0 = desactive, 1 = active, defaut 0"
	@$(ECHO) "\tTATT_N=         0 = desactive, 1 = active, defaut 0"
	@$(ECHO) "\tALL=n           met tout les T* a n"
	@$(ECHO) "\tTLINE=n         affiche la ligne n"
	@$(ECHO) "\tDLINE_IN_LINE=n affiche jusqu'a la ligne n"
	@$(ECHO) "\tLIBPRINTF=path  defaut: path = ../ft_printf"
	@$(ECHO) "Note:"
	@$(ECHO) "\tCe makefile modifie le CFLAGS du makefile de ft_printf,"
	@$(ECHO) "\tvoir documentation makefile sur override."


.PHONY: all re clean fclean
.PHONY: diff test db gcov gprof mem align help
