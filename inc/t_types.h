/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_types.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/25 10:08:45 by mmichel           #+#    #+#             */
/*   Updated: 2016/02/10 17:22:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_TYPES_H
# define T_TYPES_H

# define BSHL(x,b) ((x) << (b))
# define BOR(a,b) ((a) | (b))
# define B(b) BSHL(1, (b))

# define FT_LL long long
# define FT_UL unsigned long
# define FT_ULL unsigned long long
# define FT_LI FT_UL int
# define FT_LLI FT_ULL int
# define FT_ULI FT_UL int
# define FT_ULLI FT_ULL int
# define FT_UC unsigned char
# define FT_UI unsigned int
# define FT_SI short int
# define FT_US unsigned short
# define FT_USI unsigned short int
# define FT_UNSIGNED unsigned

typedef long double		t_ld;

typedef FT_UC			t_uc;
typedef FT_US			t_us;
typedef FT_UI			t_ui;
typedef FT_UL			t_ul;
typedef FT_ULL			t_ull;
typedef FT_LL			t_ll;

typedef FT_USI			t_usi;
typedef FT_ULI			t_uli;
typedef FT_ULLI			t_ulli;

typedef FT_UI			t_si;
typedef FT_LI			t_li;
typedef FT_LLI			t_lli;

typedef FT_UNSIGNED		t_u;

#endif
