
#ifndef FT_ENCODE_H
# define FT_ENCODE_H

# define UTF8 1

# define ENCODAGE UTF8

# if ENCODAGE == NONE
#  define FT_ENCODE(mstr, len) mstr
# elif ENCODAGE == UTF8
#  define FT_ENCODE(mstr, len) ft_encode_utf8(mstr, len)
# else
#  error Encode not supported
# endif

#endif
