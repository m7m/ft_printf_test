
#ifndef FT_PRINTF_TEST_H
# define FT_PRINTF_TEST_H

# include <stdio.h>
# include <locale.h>
# include <stddef.h>
# include <string.h>
# include <wchar.h>
# include <stdlib.h>
# include <stdarg.h>
# include <limits.h>
# include <time.h>
# include <float.h>
# include <stdint.h>

# include "ft_debug.h"
# include "ft_os.h"

# define MALLOC_CHECK_ 3

# define COLOR 0

# ifndef FT_PRINTF_TEST
#  define FT_PRINTF_TEST FT_PRINTF_TEST_FULL
# endif

# define FT_PRINTF_TEST_FMT(fmt, ...)								\
	DEBUG;															\
	i = 0; j = 0;													\
	PRCOLOR;														\
	printf("%-4d ft_printf: ", __LINE__);fflush(stdout);			\
	ft_putstr_width("fmt = [" fmt "]", 28);							\
	printf(" -> [");fflush(stdout);									\
	j = ft_printf(fmt, ##__VA_ARGS__);								\
	if (j < 30)														\
    {printf("]%*s\tret = %d\n", 30 - j, " ", j);fflush(stdout);}	\
	else															\
    {printf("]\tret = %d\n", j);fflush(stdout);}					\
																	\
	printf("%-7d printf: ", __LINE__);fflush(stdout);				\
	ft_putstr_width("fmt = [" fmt "]", 28);							\
	printf(" -> [");fflush(stdout);									\
	i = printf(fmt, ##__VA_ARGS__);fflush(stdout);					\
	if (i < 30)														\
    {printf("]%*s\tret = %d\n", 30 - i, " ", i);fflush(stdout);}	\
	else															\
    {printf("]\tret = %d\n", i);fflush(stdout);}					\
	printf(COLORF);													\
	if (__LINE__ >= DLINE)											\
		return ;

# define FT_PRINTF_TEST_FULL(fmt, ...)									\
	if (__LINE__ >= DLINE || __LINE__ <= DLINE_IN_LINE)					\
    {																	\
		DEBUG;															\
		i = 0; j = 0;													\
		PRCOLOR;														\
		printf("%-4d ft_printf: ", __LINE__);fflush(stdout);			\
		ft_putstr_width("[" fmt "]", 25);								\
		ft_putstr_width(" args = [" #__VA_ARGS__ "]", 30);				\
		printf(" -> ~");fflush(stdout);									\
		j = ft_printf(fmt, ##__VA_ARGS__);								\
		if (j < 60)														\
		{																\
			printf("~%*s\tret = %d\n", 60 - j, " ", j);fflush(stdout);	\
		}																\
		else															\
		{																\
			printf("~\tret = %d\n", j);fflush(stdout);					\
		}																\
		printf("%-7d printf: ", __LINE__);fflush(stdout);				\
		ft_putstr_width("[" fmt "]", 25);								\
		ft_putstr_width(" args = [" #__VA_ARGS__ "]", 30);				\
		printf(" -> ~");												\
		i = printf(fmt, ##__VA_ARGS__);fflush(stdout);					\
		if (i < 60)														\
		{																\
			printf("~%*s\tret = %d\n", 60 - i, " ", i);fflush(stdout);	\
		}																\
		else															\
		{																\
			printf("~\tret = %d\n", i);fflush(stdout);					\
		}																\
		printf(COLORF);													\
    }																	\
	if (__LINE__ >= DLINE)												\
		return ;

#define FT_PRINTF_ONLY_FT(fmt, ...)						\
	if (__LINE__ >= DLINE || __LINE__ <= DLINE_IN_LINE)	\
	{													\
		DEBUG;											\
		i = ft_printf(fmt, ##__VA_ARGS__);				\
		ft_putchar('\n');								\
	}													\
	if (__LINE__ >= DLINE)								\
		return ;


#define FT_PRINTF_ONLY_PRINTF(fmt, ...)			\
	DEBUG;										\
	i = printf(fmt, ##__VA_ARGS__);fflush(stdout);

# ifndef SETLOCAL_ENABLE
#  define SETLOCAL_ENABLE 1
# endif


# ifndef DLINE
#  define DLINE 5000
# else
#  ifndef DLINE_IN_LINE
#   define DLINE_IN_LINE 0
#  endif
# endif
# ifndef DLINE_IN_LINE
#  define DLINE_IN_LINE 5000
# endif


# ifndef TFLOAT
#  define TFLOAT 0
# endif
# ifndef TDIBOUX
#  define TDIBOUX 0
# endif
# ifndef TSC
#  define TSC 0
# endif
# ifndef TDOL
#  define TDOL 0
# endif
# ifndef TETOI
#  define TETOI 0
# endif
# ifndef TEXTRA
#  define TEXTRA 0
# endif

#if COLOR == 1
# define COLORD "\033["
# define COLORM "m"
# define COLORF "\033[0m"
# define PRCOLOR printf(COLORD "%d" COLORM, (__LINE__ % 2))
#else
# define PRCOLOR
# define COLORF ""
#endif


size_t	ft_strlen(const char *s);
int		ft_printf(const char *format, ...);
void    ft_putstr_width(const char *str, int n);
void    ft_putchar(char c);

void	test_float(int i, int j);
void	test_float_extra(int i, int j);
void	test_di(int i, int j);
void	test_di_extra(int i, int j);
void	test_oux(int i, int j);
void	test_oux_extra(int i, int j);

void    test_att_et(int i, int j);
void    test_att_dollar(int i, int j);
void    test_s(int i, int j);
void    test_s_extra(int i, int j);


#endif
