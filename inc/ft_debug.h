
#ifndef FT_DEBUG_H
# define FT_DEBUG_H

void	ft_debug_common(const char *fonc, int li, const char *c, const char *n);
void	ft_debug(const char *fonc, int line);
void	ft_debugstr(const char *s);
void	ft_debugnbr(int n);
void	ft_debugc(const char c);
void	ft_debugptr(void *ptr);

# ifdef DEBUG
#  undef DEBUG
#  define DEBUGIDEM __FUNCTION__, __LINE__, 
#  define DEBUG ft_debug_common(DEBUGIDEM "\n", NULL);
#  define DEBUGCOM(x) ft_debug_common(DEBUGIDEM ":", #x)
#  define DEBUGSTR(x) DEBUGCOM(x);ft_debugstr(x)
#  define DEBUGNBR(x) DEBUGCOM(x);ft_debugnbr(x)
#  define DEBUGC(x) DEBUGCOM(x);ft_debugc(x)
#  define DEBUGPTR(x) DEBUGCOM(x);ft_debugptr(x)
# else
#  undef DEBUG
#  define DEBUG
#  define DEBUGSTR(x)
#  define DEBUGNBR(x)
#  define DEBUGC(x)
#  define DEBUGPTR(x)
# endif

#endif
