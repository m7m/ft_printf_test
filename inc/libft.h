/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 01:19:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/02/18 08:39:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdint.h>
# include "t_types.h"
# include "ft_encode.h"
# include "ft_debug.h"
# include "ft_os.h"

# define MSG_OUT_OF_MEMORY "Out-of memory."
# define OUT_OF_MEMORY write(STDERR_FILENO, MSG_OUT_OF_MEMORY, 14)

int					ft_sqrt(int n);
int					ft_sqrt_arr_sup(int n);
double				ft_pow(double x, double y);
long double			ft_powl(long double x, long double y);
int					ft_isnan(char **nan, double n);

int					ft_intlen(unsigned int n);
size_t				ft_strlen(const char *s);
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncpy(char *dst, const char *src, size_t n);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strncat(char *s1, const char *s2, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
char				**ft_strsplit(char const *s, char c);

int					ft_atoi(const char *str);
double				ft_atol(const char *str);

wchar_t				*ft_encode_utf8(wchar_t *mstr, size_t len);
char				*ft_pwctoa(wchar_t *wc);
char				*ft_wctoa(wchar_t wc);
char				*ft_ctoa_base(char n, char *base);
char				*ft_uctoa_base(t_uc n, char *base);
char				*ft_itoa(int n);
char				*ft_uitoa(unsigned int n);
char				*ft_ltoa(long n);
char				*ft_ultoa(t_ul n);
char				*ft_itoa_base(int n, char *base);
char				*ft_uitoa_base(t_ui n, char *base);
char				*ft_ustoa_base(t_us n, char *base);
char				*ft_stoa_base(short n, char *base);
char				*ft_ultoa_base(t_ul n, char *base);
char				*ft_ltoa_base(long n, char *base);
char				*ft_lltoa_base(t_ll n, char *base);
char				*ft_ulltoa_base(t_ull n, char *base);
char				*ft_imtoa_base(intmax_t n, char *base);
char				*ft_uimtoa_base(uintmax_t n, char *base);
void				ft_itoa_s(int n, char *str);
void				ft_uitoa_s(unsigned int n, char *str);

int					ft_dto_str(double n, char *str, int expo, int preci);
int					ft_d_ret_expo(double n);
char				*ft_dtoa(double n, int precis);
int					ft_dtoa_expo(double *n);
char				*ft_dtoa_gg(double n, int precis);
char				*ft_dtoa_scien(double n, int precis);
char				*ft_dtoa_base16(double n, int precis);
char				*ft_hexdump(void *n, size_t size);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_isupper(int c);
int					ft_islower(int c);

int					ft_toupper(int c);
int					ft_tolower(int c);

char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int					ft_strequ(const char *s1, const char *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strdup(const char *s1);
char				*ft_strresize(char *s, size_t n);
int					ft_strocu(const char *str);

void				ft_putchar(char c);
void				ft_putnchar(char c, int n);
void				ft_putnbr(int n);
void				ft_putnbrl(long n);
void				ft_putnbrul(t_ul n);
void				ft_putstr(char const *s);
void				ft_putstrf(char const *s, char fc);
void				*ft_puterror(char const *s);
void				ft_putstr_width(const char *str, int n);
void				ft_putnbr_width(int nb, int n);

void				ft_putendl(char const *s);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);

void				*ft_memalloc(size_t size);
void				*ft_memallocset(size_t size, int c);
void				ft_memdel(void **ap);
void				*ft_memset(void *b, int c, size_t len);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				**ft_memalloc_array(size_t type, size_t slign, size_t scol);
void				ft_bzero(void *s, size_t n);
void				*ft_memdup(void *s, size_t n);

void				ft_atexit(void *ptrfree);
//void				ft_exit(int status, char *msg);
void				ft_exit(int status, char *msg);

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

t_list				*ft_lstnew(void const *content, size_t content_size);
void				ft_lstaddnext(t_list *ls,
									void const *content, size_t content_size);
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));

#endif
